import React from 'react'
import './Entry.css'
function Entry() {
    return (
        <>
            <div className="processes">
                <div className="table">
                    <table className="process-table">
                        <td>
                            <h4>Process ID</h4>
                            <tr>123123</tr>
                            <tr>56456</tr>
                            <tr>4564567</tr>
                            <tr>89782</tr>
                        </td>
                        <td>
                            <h4>SubProcess Code</h4>
                            <tr>123123</tr>
                            <tr>42345</tr>
                            <tr>564367</tr>
                            <tr>45673</tr>
                        </td>
                        <td>
                            <h4>Sub-Process Name</h4>
                            <tr>Process1</tr>
                            <tr>Process1</tr>
                            <tr>Process1</tr>
                            <tr>Process1</tr>
                        </td>
                        <td>
                            <h4> Sub-Process Heading</h4>
                            <tr>Process3</tr>
                            <tr>Process4</tr>
                            <tr>Process5</tr>
                            <tr>Process6</tr>
                        </td>
                        <td>
                            <h4> PreCaution</h4>
                            <tr>caution</tr>
                            <tr>-</tr>
                            <tr>-</tr>
                            <tr>-</tr>
                        </td>
                        <td>
                            <h4>Image Video Location</h4>
                            <tr>chennai</tr>
                            <tr>chennai</tr>
                            <tr>chennai</tr>
                            <tr>chennai</tr>
                        </td>
                    </table>
                </div>
            </div>
        </>
    )
}

export default Entry
