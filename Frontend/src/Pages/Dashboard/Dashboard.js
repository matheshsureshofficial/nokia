import React, { useState, useEffect } from 'react'
import { Accordion, Card } from 'react-bootstrap';
import SolderTable from '../../Components/Tables/SolderTable/SolderTable';
import VacuumTable from '../../Components/Tables/VacuumTable/VacuumTable';
import './Dashboard.css'
import { AiOutlineDashboard, AiOutlineDownload } from 'react-icons/ai'
import { FiChevronDown } from 'react-icons/fi'
import { FcDataSheet, FcAutomatic } from 'react-icons/fc'
// import ReportTable from '../Tables/ReportTable/ReportTable';
import * as FA from "react-icons/fa"
import { Link } from 'react-router-dom'
import OtaTable from '../../Components/Tables/OtaTable/OtaTable';
import UwaTable from '../../Components/Tables/UwaTable/UwaTable';
import VaccumeTables from '../../Components/Tables/ReportTable/VaccumeTable';
import UwaTables from '../../Components/Tables/ReportTable/UwaTable';
import OtaTables from '../../Components/Tables/ReportTable/OtaTable';
import SolderingTables from '../../Components/Tables/ReportTable/SolderingTable';
import PvaTable from "../../Components/Tables/PvaTable/PvaTable"
import PvaTables from "../../Components/Tables/ReportTable/PvaTable"
import Machinecreate from '../../Components/Create/Machine.jsx'
import MachineTable from '../../Components/Tables/MachineTable/MachineTable'
import "animate.css"
import { checkLogin } from "../../auth"
import { useHistory } from "react-router-dom"
function Dashboard() {
    const history = useHistory()
    const [showTable, setshowTable] = useState(0)
    const [showreportTable, setshowreportTable] = useState(0)
    const [showmachineTable, setshowmachineTable] = useState(1)
    const [table, settable] = useState(true)
    const [machine, setmachine] = useState(false)
    const [report, setreport] = useState(false)
    const [activemachine, setactivemachine] = useState(0)
    const [inactivemachine, setinactivemachine] = useState(0)
    const [missedmachine, setmissedmachine] = useState(0)

    // useEffect(() => {
    //     async function loginCheck() {
    //         const checklogin = await checkLogin()
    //         if (checklogin === false) {
    //             history.push("/login")
    //         }
    //     }
    //     loginCheck()
    // }, [])
    const cardBottom = {
        position: "absolute",
        background: "rgba(255,255,255,0.25)",
        width: "100%",
        left: "0",
        bottom: "0",
        height: "30px",
        textAlign: "left",
        paddingLeft: "12px", paddingTop: "3px"
    }
    const tablebtn = () => {
        settable(true)
        setmachine(false)
        setreport(false)
    }
    const machinebtn = () => {
        settable(false)
        setmachine(true)
        setreport(false)
    }
    const reportbtn = () => {
        setreport(true)
        setmachine(false)
        settable(false)
    }

    const machineStatus = (active, inactive, missed) => {
        setactivemachine(active)
        setinactivemachine(inactive)
        setmissedmachine(missed)
    }
    return (
        <>
            <div className='bg-light dashboard-container'>
                <div className='dashbord-wrap col-2'>
                    <Link to="/"> <div className='dashboard-logo border-bottom'>NOKIA</div></Link>
                    <div className='dashboard-menu-list'><AiOutlineDashboard className='dashboard-icons' /> Dashboard</div>
                    <Accordion defaultActiveKey="0">
                        <Accordion.Toggle as={Card.Header} eventKey="1" className='dashboard-menu-list' onClick={reportbtn} >
                            <AiOutlineDownload className='dashboard-icons' />Reports<FiChevronDown className='accordion-menu-down-arrow' />
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="1">
                            <ul className='dashboard-accordion-submenu'>
                                <li onClick={() => setshowreportTable(0)} >Solder tip temperature</li>
                                <li onClick={() => setshowreportTable(1)} >vacuum lifter</li>
                                <li onClick={() => setshowreportTable(2)}>OTA</li>
                                <li onClick={() => setshowreportTable(3)}>UWA</li>
                                <li onClick={() => setshowreportTable(4)}>PVA</li>
                            </ul>
                        </Accordion.Collapse>
                        <Accordion.Toggle as={Card.Header} eventKey="0" className='dashboard-menu-list' onClick={tablebtn}>
                            <FcDataSheet className='dashboard-icons' />Tables  <FiChevronDown className='accordion-menu-down-arrow' />
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                            <ul className='dashboard-accordion-submenu'>
                                <li onClick={() => setshowTable(0)}>Solder tip temperature</li>
                                <li onClick={() => setshowTable(1)}>vacuum lifter</li>
                                <li onClick={() => setshowTable(2)}>OTA</li>
                                <li onClick={() => setshowTable(3)}>UWA</li>
                                <li onClick={() => setshowTable(4)}>PVA</li>
                            </ul>
                        </Accordion.Collapse>

                        <Accordion.Toggle as={Card.Header} eventKey="2" className='dashboard-menu-list' onClick={machinebtn}>
                            <FcAutomatic className='dashboard-icons' />Machine  <FiChevronDown className='accordion-menu-down-arrow' />
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="2">
                            <ul className='dashboard-accordion-submenu'>
                                <li onClick={() => setshowmachineTable(0)}>Add New Machine</li>
                                <li onClick={() => setshowmachineTable(1)}>All Machine</li>
                            </ul>
                        </Accordion.Collapse>
                    </Accordion>
                </div>
                <div className="dashboard-section col-10 animate__animated animate__zoomIn">
                    {
                        !machine ? <div className="d-flex justify-content-center mt-4">
                        <div className="col-md-3">
                            <div className="card-counter primary position-relative">
                                <FA.FaCodeBranch style={{ fontSize: "40px" }} />
                                <span className="count-numbers">{inactivemachine}</span>
                                <span className="count-name">InComplete</span>
                                <span style={cardBottom}>TODAY</span>
                            </div>
                        </div>

                        <div className="col-md-3">
                            <div className="card-counter danger position-relative">
                                <FA.FaCog className="fa fa-spin" style={{ fontSize: "50px" }} />
                                <span className="count-numbers">{missedmachine}</span>
                                <span className="count-name">Missed</span>
                                <span style={cardBottom}>TODAY</span>
                            </div>
                        </div>

                        <div className="col-md-3">
                            <div className="card-counter info position-relative">
                                <FA.FaRegChartBar style={{ fontSize: "50px" }} />
                                <span className="count-numbers">{activemachine}</span>
                                <span className="count-name">Completed</span>
                                <span style={cardBottom}>TODAY</span>
                            </div>
                        </div>
                    </div>:<div></div>
                    }
                    
                    <div className="mt-5">
                        {
                            table ? showTable === 0 ? <SolderTable machineStatus={machineStatus} /> : showTable === 1 ? <VacuumTable machineStatus={machineStatus} /> : showTable === 2 ? <OtaTable machineStatus={machineStatus} /> : showTable === 3 ? <UwaTable machineStatus={machineStatus} /> : showTable === 4 ? <PvaTable machineStatus={machineStatus} /> : null : null
                        }
                        {
                            report ? showreportTable === 0 ? <SolderingTables machineStatus={machineStatus} /> : showreportTable === 1 ? <VaccumeTables machineStatus={machineStatus} /> : showreportTable === 2 ? <OtaTables machineStatus={machineStatus} /> : showreportTable === 3 ? <UwaTables machineStatus={machineStatus} /> : showreportTable === 4 ? <PvaTables machineStatus={machineStatus} /> : null : null
                        }
                        {
                            machine ? showmachineTable === 0 ? <Machinecreate /> : <MachineTable /> : null
                        }
                    </div>

                    {/* <div className="mt-5">
                        {
                            solderTable ? <SolderTable /> : vacTab ? <VacuumTable /> : null
                        }
                    </div> */}
                    {/* <div className='d-flex mt-5 mb-4 justify-content-center'>
                        <button className='solderbtn mr-3' onClick={() => setSolderTable(true)}>Solder Report</button>
                        <button className='vacuumbtn' onClick={() => setSolderTable(false)}>Vacuum Report</button>
                    </div><div className='d-flex mt-5 mb-4 justify-content-center'>
                        <button className='solderbtn mr-3' onClick={() => setSolderTable(true)}>Solder Report</button>
                        <button className='vacuumbtn' onClick={() => setSolderTable(false)}>Vacuum Report</button>
                    </div>
                    {
                        solderTable ? <SolderTable /> : <VacuumTable />
                    } */}
                </div>

            </div>
        </>
    )
}

export default Dashboard
