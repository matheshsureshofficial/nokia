import axios from 'axios'
import React, { Component } from 'react'
import { Table } from 'react-bootstrap'
import moment from "moment"
import Paginatnation from "../../pagination"

export default class PvaTable extends Component {
    constructor(props) {
        super()
        this.state = {
            uwa: [],
            currentPage: 1,
            postsPerPage: 15
        }
    }
    componentDidMount = async () => {
        var today = new Date();
        const hours = today.getHours() + ":" + today.getMinutes()
        const shiftdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/pva`).then((res) => {
            return res.data
        })
        if (hours < "14:30") {
            const shiftreport = await this.shiftReport(shiftdata, "Shift A")
            this.setState({
                uwa: shiftreport
            })
            this.inActiveMachines("Shift A")
        } else if (hours > "14.30" && hours < "22.30") {
            const shiftreport = await this.shiftReport(shiftdata, "Shift B")
            this.setState({
                uwa: shiftreport
            })
            this.inActiveMachines("Shift B")
        } else {
            const shiftreport = await this.shiftReport(shiftdata, "Shift C")
            this.setState({
                uwa: shiftreport
            })
            this.inActiveMachines("Shift C")
        }
    }
    shiftReport = async (shiftdata, shift) => {
        var today = new Date();
        const monthsandday = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
            "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60"]
        var todaydate = today.getFullYear() + '-' + (monthsandday[today.getMonth() + 1]) + '-' + (monthsandday[today.getDate()]);
        const filtershift = await shiftdata.filter((shifts, index) => { return shifts.shift === shift && shifts.date.includes(todaydate) })
        return filtershift
    }
    notEntrymachines = async (machineData, machinenos) => {
        if (machineData.length === 0) return { inactive: machinenos, active: [] }
        var notEntrymachine = [], active = []
        for (var i = 0; i < machinenos.length; i++) {
            var machinedata = await machineData.filter((data) => { return data.machine_Sl_No === machinenos[i] })
            if (machinedata.length === 0) {
                notEntrymachine.push(machinenos[i])
            } else {
                active.push(machinenos[i])
            }
        }
        return { inactive: notEntrymachine, active: active }
    }

    inActiveMachines = async (todayshift) => {
        const { uwa: shiftdata } = this.state
        const complete = await shiftdata.filter((data) => data.status === "Complete")
        const incomplete = await shiftdata.filter((data) => data.status === "In Complete")
        const { machineStatus } = this.props
        machineStatus(complete.length, incomplete.length, 0)
    }
    paginate = (pageNumber) => {
        this.setState({ currentPage: pageNumber })
    }
    render() {
        const { uwa } = this.state
        const { currentPage, postsPerPage } = this.state
        const indexOfLastPost = currentPage * postsPerPage;
        const indexOfFirstPost = indexOfLastPost - postsPerPage;
        const currentPosts = uwa.slice(indexOfFirstPost, indexOfLastPost);
        return (
            <>
                <h3 className='text-center mb-4 text-dark' style={{ marginBottom: "10px !important" }}>Testers Checklist PVA</h3>
                <Table striped bordered hover dir="ltr" border="1" cellSpacing="0" cellPadding="0"><colgroup><col width="100" /><col width="100" /><col width="128" /><col width="100" /><col width="100" /><col width="117" /><col width="100" /></colgroup>
                    <tbody>
                        <tr style={{ height: "24px", backgroundColor: "#124191", color: "#fff" }}>
                            <td style={{ height: "24px", textAlign: "center" }} data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Date&quot;}">Date</td>
                            <td style={{ height: "24px", textAlign: "center" }} data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Shift&quot;}">Shift</td>
                            <td style={{ height: "24px", textAlign: "center" }} data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Type of Equipment&quot;}">Type of Equipment</td>
                            <td style={{ height: "24px", textAlign: "center" }} data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Operator&quot;}">Operator</td>
                            <td style={{ height: "24px", textAlign: "center" }} data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Machine ID&quot;}">Machine ID</td>
                            <td style={{ height: "24px", textAlign: "center" }} data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Status&quot;}">Status</td>
                            <td style={{ height: "24px", textAlign: "center" }} data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Issue Stage&quot;}">Issue Stage</td>
                            <td style={{ height: "24px", textAlign: "center" }} data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Issue Stage&quot;}">Average</td>
                        </tr>
                        {currentPosts.length !== 0 ? currentPosts.map((uwatable, index) => (
                            <tr style={{ height: "13px" }} key={index}>
                                <td style={{ height: "13px", textAlign: "center" }}>{"Today"}</td>
                                <td style={{ height: "13px", textAlign: "center" }}>{uwatable.shift}</td>
                                <td style={{ height: "13px", textAlign: "center" }}>{uwatable.machine_Sl_No}</td>
                                <td style={{ height: "13px", textAlign: "center" }}>{uwatable.checked_by}</td>
                                <td style={{ height: "13px", textAlign: "center" }}>{uwatable.machine_Sl_No}</td>
                                <td style={{ height: "13px", textAlign: "center" }}>{uwatable.status}</td>
                                <td style={{ height: "13px", textAlign: "center" }}>{uwatable.status !== "Complete" ? uwatable.status === "In Active" ? "-" : uwatable.statuslists : "-"}</td>
                                <td style={{ height: "13px", textAlign: "center" }}>{uwatable.average}</td>

                            </tr>
                        )) : null
                        }

                    </tbody>
                </Table>
                {
                    currentPosts.length === 0 ? <div className="text-center">No data</div> : null
                }
                <div style={{ float: "right" }} >
                    {
                        uwa.length >=15 ? <Paginatnation currentPage={currentPage} postsPerPage={postsPerPage}
                            totalPosts={uwa.length}
                            paginate={this.paginate} /> : <div></div>
                    }
                </div>
            </>
        )
    }
}
