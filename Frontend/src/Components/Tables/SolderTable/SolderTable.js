import axios from 'axios'
import React, { Component } from 'react'
import { Table } from 'react-bootstrap'
import { Bar } from 'react-chartjs-2';
import moment from "moment"
import Paginatnation from "../../pagination"
export default class SolderTable extends Component {
    constructor(props) {
        super()
        this.state = {
            soldering: [],
            from: null,
            to: null,
            chartdate: "",
            stations: [],
            counts: [],
            shift: null,
            status: null,
            currentPage: 1,
            postsPerPage: 15
        }
    }
    componentDidMount = async () => {
        var today = new Date();
        const hours = today.getHours() + ":" + today.getMinutes()
        const shiftdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/soldering`).then((res) => {
            return res.data
        })
        if (shiftdata.length !== 0) {
            const todayData = await shiftdata.filter((data) => data.date === moment().format("YYYY-MM-DD"))
            this.setState({
                soldering: todayData
            })
            this.drawChart()
        }
        if (hours < "14:30") {
            this.inActiveMachines("Shift A")
        } else if (hours > "14.30" && hours < "22.30") {
            this.inActiveMachines("Shift B")
        } else {
            this.inActiveMachines("Shift C")

        }
    }
    shiftfilter = async (e) => {
        const shift = e.target.value
        const shiftdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/soldering`).then((res) => {
            return res.data
        })
        if (shift !== "none") {
            const filtershift = await shiftdata.filter((shifts, index) => { return shifts.shift === shift })
            this.setState({
                soldering: filtershift,
                shift: shift
            })
            this.drawChart()
        } else {
            this.setState({
                soldering: shiftdata
            })
            this.drawChart()
        }
    }
    notEntrymachines = async (machineData, machinenos) => {
        if (machineData.length === 0) return { inactive: machinenos, active: [] }
        var notEntrymachine = [], active = []
        for (var i = 0; i < machinenos.length; i++) {
            var machinedata = await machineData.filter((data) => { return data.machine_Sl_No === machinenos[i] })
            if (machinedata.length === 0) {
                notEntrymachine.push(machinenos[i])
            } else {
                active.push(machinenos[i])
            }
        }
        return { inactive: notEntrymachine, active: active }
    }


    inActiveMachines = async (todayshift) => {
        const { soldering: shiftdata } = this.state
        const complete = await shiftdata.filter((data) =>  data.status === "Complete")
        const incomplete = await shiftdata.filter((data) =>  data.status === "In Complete")
        const { machineStatus } = this.props
        machineStatus(complete.length, incomplete.length, 0)
    }
    hanlechange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    datefilter = async () => {
        const { from, to, shift, status } = this.state
        const soldering = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/soldering`).then((res) => {
            return res.data
        })
        if (from == null) {
            alert("From Date Requires")
            return false
        } else if (to == null) {
            alert("To Date Requires")
            return false
        } else {
            if (soldering.length !== 0) {
                var finalFilter = []
                const filterDate = await soldering.filter((data) => data.date >= from && data.date <= to)
                if (shift !== null) {
                    finalFilter = await filterDate.filter((data) => data.shift === shift)
                } else if (status !== null) {
                    finalFilter = await filterDate.filter((data) => data.status === status)
                } else {
                    finalFilter = filterDate
                }
                this.setState({ soldering: finalFilter })
                this.drawChart()
            }
        }
    }
    Resetfilter = async () => {
        const resetdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/soldering`).then((res) => {
            return res.data
        })
        this.setState({
            soldering: resetdata
        })
        this.drawChart()
    }
    exportdata = async () => {
        const exportsoldering = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/soldering/export`).then((res) => {
            return res.data
        })
        window.open(exportsoldering)
    }
    statusfilter = async (e) => {
        const status = e.target.value
        const statusdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/soldering`).then((res) => {
            return res.data
        })
        if (status !== "none") {
            const filterstatus = await statusdata.filter((statuss, index) => { return statuss.status === status })
            this.setState({
                soldering: filterstatus,
                status: status
            })
            this.drawChart()
        } else {
            this.setState({
                soldering: statusdata
            })
            this.drawChart()
        }
    }

    drawChart = async () => {
        const { soldering } = this.state
        if (soldering.length !== 0) {
            var countdata = [], alldates = []
            var labels = [], MaxTemp = [], MinTemp = [], acttemp = []
            for (var i = 0; i < soldering.length; i++) {
                labels.push(soldering[i].station)
                var maxtemp = Number(soldering[i].defaultTemp) + Number(10)
                var mintemp = Number(soldering[i].defaultTemp) - Number(10)
                MaxTemp.push(maxtemp)
                MinTemp.push(mintemp)
                acttemp.push(soldering[i].temperature)
                alldates.push(soldering[i].date)
            }
            const originalDate = [...new Set(alldates)]
            for (var j = 0; j < originalDate.length; j++) {
                var dateData = await soldering.filter((data) => data.date === originalDate[j])
                countdata.push(dateData.length)
            }
            const chartData = {
                labels: labels,
                MaxTemp: MaxTemp,
                MinTemp: MinTemp,
                acttemp: acttemp
            }
            this.setState({ stations: chartData, counts: { date: originalDate, count: countdata } })
        } else {
            const chartData = {
                labels: ["Null"],
                MaxTemp: [0],
                MinTemp: [0],
                acttemp: [0]
            }
            this.setState({ stations: chartData, counts: { date: ["null"], count: [0] } })
        }
    }
    paginate = (pageNumber) => {
        this.setState({ currentPage: pageNumber })
    }
    render() {
        const { soldering, stations, counts } = this.state
        const { MaxTemp, MinTemp, acttemp, labels } = stations
        const { date, count } = counts
        const data = {
            labels: labels,
            datasets: [
                {
                    type: 'line',
                    label: 'MaxTemp',
                    borderColor: 'rgb(54, 162, 235)',
                    borderWidth: 2,
                    fill: false,
                    data: MaxTemp,
                },
                {
                    type: 'bar',
                    label: 'Actual Temp',
                    backgroundColor: 'rgb(75, 192, 192,0.5)',
                    data: acttemp,
                },
                {
                    type: 'line',
                    label: 'MinTemp',
                    backgroundColor: 'rgb(255, 99, 132)',
                    data: MinTemp,
                    borderColor: 'red',
                    borderWidth: 2,
                }

            ],
        };
        var options = {
            scales: {
                y: {
                    min: 0,
                    max: 50,
                }
            }
        };
        var options1 = {
            scales: {
                y: {
                    min: 300,
                    max: 500,
                }
            }
        };
        const countdata = {
            labels: date,
            datasets: [
                {
                    type: 'bar',
                    label: 'Count',
                    backgroundColor: 'rgb(75, 192, 192,0.5)',
                    data: count,
                },

            ],
        };
        const { currentPage, postsPerPage } = this.state
        const indexOfLastPost = currentPage * postsPerPage;
        const indexOfFirstPost = indexOfLastPost - postsPerPage;
        const currentPosts = soldering.slice(indexOfFirstPost, indexOfLastPost);


        return (
            <>
                <div className='p-3 container-fluid'>
                    <h3 className='text-center mb-4' style={{ marginBottom: "10px !important" }}>Solder Tip Temperature Monitoring</h3>
                    <div className='d-flex justify-content-between p-5'>
                        <div style={{ width: '450px', height: '450px' }} className='p-3 m-4'>
                            <Bar data={data} options={options1} />
                        </div>
                        {/* <Chart
                            width={'500px'}
                            height={'300px'}
                            chartType="Bar"
                            loader={<div>Loading Chart</div>}
                            data={counts}
                            options={{
                                // Material design options
                                chart: {
                                    title: 'Soldering Complaince Report',
                                    subtitle: 'Datewise / Shiftwise',
                                },
                                colors: ['#2b78e3', '#ff9326'],
                            }}
                            // For tests
                            rootProps={{ 'data-testid': '2' }}
                        /> */}
                        <div style={{ width: '450px', height: '450px' }} className='p-3 m-4'>
                            <Bar data={countdata} options={options} />
                        </div>
                    </div>
                    <div className='d-flex justify-content-between my-2'>
                        <div className="d-flex">
                            <div className="pt-1">
                                <label htmlFor="from" className='mr-2'>From</label>
                                <input type="date" id="from" name="from" onChange={e => this.hanlechange(e)} />
                            </div>
                            <div className="pt-1 px-3">
                                <label htmlFor="to" className='mr-2'>To</label>
                                <input type="date" id="to" name="to" onChange={e => this.hanlechange(e)} />
                            </div>
                            <div className="pt-1 px-3">
                                <select className="form-select mr-1" onChange={e => this.shiftfilter(e)}>
                                    <option value="none">Filter By Shift</option>
                                    <option value="Shift A">Shift A</option>
                                    <option value="Shift B" >Shift B</option>
                                    <option value="Shift D">Shift C</option>
                                </select>
                            </div>
                            <div className="pt-1 px-3">
                                <select className="form-select mr-1" onChange={e => this.statusfilter(e)}>
                                    <option value="none">Filter By Status</option>
                                    <option value="Complete" >Complete</option>
                                    <option value="Incomplete">Incomplete</option>
                                </select>
                            </div>
                            <div className="search">
                                <button className="btn btn-info mr-2" onClick={this.datefilter}>Filter</button>
                                <button className="btn btn-info" onClick={this.Resetfilter}>Reset</button>
                            </div>
                        </div>
                        <div >
                            <button className="btn btn-info mr-2" onClick={this.exportdata}>Export</button>
                        </div>
                    </div>
                    <Table striped bordered hover size="lg" responsive="sm">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Shift </th>
                                <th>Soldering Station Sl No</th>
                                <th>Catridge Used / Set Temperature</th>
                                <th>Actual Temperature<br />(±10°C)</th>
                                <th>No of Catridges</th>
                                <th>Remeasure</th>
                                <th>Checked by</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                currentPosts && currentPosts.map((solder, index) => (
                                    <tr key={index}>
                                        <td>{solder.date}</td>
                                        <td>{solder.shift}</td>
                                        <td>{solder.station}</td>
                                        <td>{solder.catridge_used}</td>
                                        <td>{solder.temperature}</td>
                                        <td>{solder.replacecount}</td>
                                        <td>{solder.remeasurecount}</td>
                                        <td>{solder.checked_by}</td>
                                        <td>{solder.status}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                    {
                        currentPosts.length === 0 ? <div className="text-center">No data</div> : null
                    }
                    <div style={{ float: "right" }} >
                        {
                            soldering.length >=15 ? <Paginatnation currentPage={currentPage} postsPerPage={postsPerPage}
                                totalPosts={soldering.length}
                                paginate={this.paginate} /> : <div></div>
                        }
                    </div>

                </div>
            </>
        )
    }
}

