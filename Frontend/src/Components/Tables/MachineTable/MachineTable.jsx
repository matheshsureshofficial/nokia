import axios from 'axios'
import moment from 'moment';
import React, { Component } from 'react'
// import { Table } from 'reactstrap'
import { Table } from 'react-bootstrap'
import { Bar } from 'react-chartjs-2';
import Paginatnation from "../../pagination"

export default class OtaTable extends Component {
    constructor(props) {
        super()
        this.state = {
            machine: [],
            currentPage: 1,
            postsPerPage: 15
        }
    }
    componentDidMount = async () => {

        const shiftdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/viewall`).then((res) => {
            return res.data
        })
        if (shiftdata.length !== 0) {
            this.setState({
                machine: shiftdata
            })
        }
    }


    shiftfilter = async (e) => {
        const shift = e.target.value
        const shiftdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/viewall`).then((res) => {
            return res.data
        })
        if (shift !== "none") {
            const filtershift = await shiftdata.filter((shifts, index) => { return shifts.table_name === shift })
            this.setState({
                machine: filtershift,
            })
        } else {
            this.setState({
                machine: shiftdata,
            })
        }

    }
    hanlechange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    Resetfilter = async () => {
        const resetdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/viewall`).then((res) => {
            return res.data
        })
        this.setState({
            machine: resetdata
        })
    }

    paginate = (pageNumber) => {
        this.setState({ currentPage: pageNumber })
    }
    render() {
        const { machine } = this.state

        const { currentPage, postsPerPage } = this.state
        const indexOfLastPost = currentPage * postsPerPage;
        const indexOfFirstPost = indexOfLastPost - postsPerPage;
        const currentPosts = machine.slice(indexOfFirstPost, indexOfLastPost);
        return (
            <>
                <div className='p-3 container-fluid'>
                    <h3 className='text-center mb-4' style={{ marginBottom: "10px !important" }}>Machine Details</h3>

                    <div className='d-flex justify-content-between my-2'>
                        <div className="d-flex">

                            <div className="pt-1 px-3">
                                <select className="form-select mr-1" onChange={e => this.shiftfilter(e)}>
                                    <option value="none">Filter By Category</option>
                                    <option value="UWA">UWA</option>
                                    <option value="OTA" >OTA</option>
                                    <option value="PVA">PVA</option>
                                    <option value="Soldering" >Soldering</option>
                                    <option value="Vaccume">Vaccume</option>
                                </select>
                            </div>

                            <div className="search">
                                <button className="btn btn-info mr-2" onClick={this.datefilter}>Filter</button>
                                <button className="btn btn-info" onClick={this.Resetfilter}>Reset</button>
                            </div>
                        </div>

                    </div>

                    <Table striped bordered hover size="sm" id="ota2" responsive="sm">
                        <thead>
                            <tr>
                                <th className="tg-54sw text-center pb-4" rowSpan="3">Category</th>
                                <th className="tg-54sw text-center pb-4" rowSpan="3">Machine Serial No</th>
                            </tr>
                        </thead>
                        <tbody>
                            {currentPosts.length !== 0 ? currentPosts.map((machine, index) => (
                                <tr key={index}>
                                    <td className="tg-54sw text-center pb-4">{machine.table_name}</td>
                                    <td className="tg-54sw text-center pb-4">{machine.serial_no}</td>
                                </tr>
                            )) : null
                            }
                        </tbody>
                    </Table>
                    {
                        currentPosts.length === 0 ? <div className="text-center">No data</div> : null
                    }
                    {
                        machine.length >=15 ? <div style={{float:"right"}}><Paginatnation currentPage={currentPage} postsPerPage={postsPerPage}
                        totalPosts={machine.length}
                        paginate={this.paginate} /></div> : <div></div>
                    }
                </div>
            </>
        )
    }
}

