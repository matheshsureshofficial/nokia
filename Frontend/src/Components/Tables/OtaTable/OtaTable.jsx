import axios from 'axios'
import moment from 'moment';
import React, { Component } from 'react'
// import { Table } from 'reactstrap'
import { Table } from 'react-bootstrap'
import { Bar } from 'react-chartjs-2';
import Paginatnation from "../../pagination"

export default class OtaTable extends Component {
    constructor(props) {
        super()
        this.state = {
            ota: [],
            from: null,
            to: null,
            chartdate: "",
            chart: [],
            shift: null,
            status: null,
            currentPage: 1,
            postsPerPage: 15
        }
    }
    componentDidMount = async () => {
        var today = new Date();
        const hours = today.getHours() + ":" + today.getMinutes()
        const shiftdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/ota`).then((res) => {
            return res.data
        })
        if (shiftdata.length !== 0) {
            const todayData = await shiftdata.filter((data) => data.date === moment().format("YYYY-MM-DD"))
            this.setState({
                ota: todayData
            })
            this.drawChart()
        }

        if (hours < "14:30") {

            this.inActiveMachines("Shift A")
        } else if (hours > "14.30" && hours < "22.30") {

            this.inActiveMachines("Shift B")
        } else {

            this.inActiveMachines("Shift C")
        }

    }
    notEntrymachines = async (machineData, machinenos) => {
        if (machineData.length === 0) return { inactive: machinenos, active: [] }
        var notEntrymachine = [], active = []
        for (var i = 0; i < machinenos.length; i++) {
            var machinedata = await machineData.filter((data) => { return data.machine_Sl_No === machinenos[i] })
            if (machinedata.length === 0) {
                notEntrymachine.push(machinenos[i])
            } else {
                active.push(machinenos[i])
            }
        }
        return { inactive: notEntrymachine, active: active }
    }

    inActiveMachines = async (todayshift) => {
        const { ota: shiftdata } = this.state
        const complete = await shiftdata.filter((data) =>  data.status === "Complete")
        const incomplete = await shiftdata.filter((data) =>  data.status === "In Complete")
        const { machineStatus } = this.props
        machineStatus(complete.length, incomplete.length, 0)
    }
    shiftfilter = async (e) => {
        const shift = e.target.value
        const shiftdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/ota`).then((res) => {
            return res.data
        })
        if (shift !== "none") {
            const filtershift = await shiftdata.filter((shifts, index) => { return shifts.shift === shift })
            this.setState({
                ota: filtershift,
                shift: shift
            })
            this.drawChart()
        } else {
            this.setState({
                ota: shiftdata,
            })
            this.drawChart()
        }

    }
    hanlechange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    datefilter = async () => {
        const { from, to, shift, status } = this.state
        const ota = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/ota`).then((res) => {
            return res.data
        })
        if (from == null) {
            alert("From Date Requires")
            return false
        } else if (to == null) {
            alert("To Date Requires")
            return false
        } else {
            if (ota.length !== 0) {
                var finalFilter = []
                const filterDate = await ota.filter((data) => data.date >= from && data.date <= to)
                if (shift !== null) {
                    finalFilter = await filterDate.filter((data) => data.shift === shift)
                } else if (status !== null) {
                    finalFilter = await filterDate.filter((data) => data.status === status)
                } else {
                    finalFilter = filterDate
                }
                this.setState({ ota: finalFilter })
                this.drawChart()
            }
        }
    }
    Resetfilter = async () => {
        const resetdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/ota`).then((res) => {
            return res.data
        })
        this.setState({
            ota: resetdata
        })
        this.drawChart()
    }
    exportdata = async () => {
        const exportota = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/ota/export`).then((res) => {
            return res.data
        })
        window.open(exportota)
    }
    statusfilter = async (e) => {
        const status = e.target.value
        const statusdata = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/ota`).then((res) => {
            return res.data
        })
        if (status !== "none") {
            const filterstatus = await statusdata.filter((statuss, index) => { return statuss.status === status })
            this.setState({
                ota: filterstatus,
                status: status
            })
            this.drawChart()
        } else {
            this.setState({
                ota: statusdata
            })
            this.drawChart()
        }
    }

    drawChart = async () => {
        const { ota } = this.state
        var Machines = [], total = [], ok = [], notok = []
        if (ota.length !== 0) {
            for (var i = 0; i < ota.length; i++) {
                var allaverage = ota[i].average
                var nokp = allaverage.split("/")[0]
                var okp = Number(10) - Number(nokp)
                Machines.push(ota[i].machine_Sl_No)
                total.push(10)
                ok.push(Number(okp))
                notok.push(Number(nokp))
            }
            this.setState({ chart: { Machines: Machines, total: total, ok: ok, notok: notok } })
        } else {
            this.setState({ chart: { Machines: ["Null"], total: [0], ok: [0], notok: [0] } })
        }
    }
    paginate = (pageNumber) => {
        this.setState({ currentPage: pageNumber })
    }
    render() {
        const { ota, chart } = this.state
        const { Machines, total, ok, notok } = chart
        const data = {
            labels: Machines,
            datasets: [
                {
                    type: 'bar',
                    label: "Total Points",
                    backgroundColor: 'rgb(75, 192, 192,0.5)',
                    data: total,
                },
                {
                    type: 'bar',
                    label: "OK Points",
                    backgroundColor: 'green',
                    data: notok,
                },
                {
                    type: 'bar',
                    label: "Not OK Points",
                    backgroundColor: 'rgb(255, 99, 132)',
                    data: ok,
                }

            ],
        };
        var options = {
            scales: {
                y: {
                    min: 0,
                    max: 15,
                }
            }
        };
        const { currentPage, postsPerPage } = this.state
        const indexOfLastPost = currentPage * postsPerPage;
        const indexOfFirstPost = indexOfLastPost - postsPerPage;
        const currentPosts = ota.slice(indexOfFirstPost, indexOfLastPost);
        return (
            <>
                <div className='p-3 container-fluid'>
                    <h3 className='text-center mb-4' style={{ marginBottom: "10px !important" }}>Testers Checklist OTA</h3>
                    <div >
                        <div style={{ width: '450px', height: '500px' }} className='p-3 m-4'>
                            <Bar data={data} options={options} width="200px" />
                        </div>
                    </div>
                    <div className='d-flex justify-content-between my-2'>
                        <div className="d-flex">
                            <div className="pt-1">
                                <label htmlFor="from" className='mr-2'>From</label>
                                <input type="date" id="from" name="from" onChange={e => this.hanlechange(e)} />
                            </div>
                            <div className="pt-1 px-3">
                                <label htmlFor="to" className='mr-2'>To</label>
                                <input type="date" id="to" name="to" onChange={e => this.hanlechange(e)} />
                            </div>
                            <div className="pt-1 px-3">
                                <select className="form-select mr-1" onChange={e => this.shiftfilter(e)}>
                                    <option value="none">Filter By Shift</option>
                                    <option value="Shift A">Shift A</option>
                                    <option value="Shift B" >Shift B</option>
                                    <option value="Shift D">Shift C</option>
                                </select>
                            </div>
                            <div className="pt-1 px-3">
                                <select className="form-select mr-1" onChange={e => this.statusfilter(e)}>
                                    <option value="none">Filter By Status</option>
                                    <option value="Complete" >Complete</option>
                                    <option value="In Complete">In Complete</option>
                                </select>
                            </div>
                            <div className="search">
                                <button className="btn btn-info mr-2" onClick={this.datefilter}>Filter</button>
                                <button className="btn btn-info" onClick={this.Resetfilter}>Reset</button>
                            </div>
                        </div>
                        <div >
                            <button className="btn btn-info mr-2" onClick={this.exportdata}>Export</button>
                        </div>
                    </div>

                    <Table striped bordered hover size="sm" id="ota2" responsive="sm">
                        <thead>
                            <tr>
                                <th className="tg-54sw text-center pb-4" rowSpan="3">Date</th>
                                <th className="tg-54sw text-center pb-4" rowSpan="3">Shift</th>
                                <th className="tg-54sw text-center pb-4" rowSpan="3">Machine Serial No</th>
                                {/* <th className="tg-54sw text-center pb-4" rowSpan="3">Pressure Guage Value</th> */}
                                <th className="tg-54sw text-center pb-4" rowSpan="3">Checked By</th>
                                <th className="tg-54sw text-center " colSpan="20">Status</th>
                                <th className="tg-wa1i text-center pb-4" rowSpan="3">Status</th>
                            </tr>
                            <tr>
                                <td className="tg-54sw text-center" colSpan="2">Process1</td>
                                <td className="tg-54sw text-center" colSpan="2">Process2</td>
                                <td className="tg-54sw text-center" colSpan="2">Process3</td>
                                <td className="tg-54sw text-center" colSpan="2">Process4</td>
                                <td className="tg-54sw text-center" colSpan="2">Process5</td>
                                <td className="tg-54sw text-center" colSpan="2">Process6</td>
                                <td className="tg-54sw text-center" colSpan="2">Process7</td>
                                <td className="tg-2g1l text-center" colSpan="2">Process8</td>
                                <td className="tg-2g1l text-center" colSpan="2">Process9</td>
                                <td className="tg-2g1l text-center" colSpan="2">Process10</td>
                            </tr>
                            <tr>
                                <td className="tg-54sw text-center" colSpan="1">Time(sec)</td>
                                <td className="tg-54sw text-center" colSpan="1">Result</td>
                                <td className="tg-54sw text-center" colSpan="1">Time(sec)</td>
                                <td className="tg-54sw text-center" colSpan="1">Result</td>
                                <td className="tg-54sw text-center" colSpan="1">Time(sec)</td>
                                <td className="tg-54sw text-center" colSpan="1">Result</td>
                                <td className="tg-54sw text-center" colSpan="1">Time(sec)</td>
                                <td className="tg-54sw text-center" colSpan="1">Result</td>
                                <td className="tg-54sw text-center" colSpan="1">Time(sec)</td>
                                <td className="tg-54sw text-center" colSpan="1">Result</td>
                                <td className="tg-54sw text-center" colSpan="1">Time(sec)</td>
                                <td className="tg-54sw text-center" colSpan="1">Result</td>
                                <td className="tg-54sw text-center" colSpan="1">Time(sec)</td>
                                <td className="tg-54sw text-center" colSpan="1">Result</td>
                                <td className="tg-54sw text-center" colSpan="1">Time(sec)</td>
                                <td className="tg-54sw text-center" colSpan="1">Result</td>
                                <td className="tg-54sw text-center" colSpan="1">Time(sec)</td>
                                <td className="tg-54sw text-center" colSpan="1">Result</td>
                                <td className="tg-54sw text-center" colSpan="1">Time(sec)</td>
                                <td className="tg-54sw text-center" colSpan="1">Result</td>
                            </tr>
                        </thead>
                        <tbody>
                            {currentPosts.length!==0 ? currentPosts.map((otainfo, index) => (
                                <tr key={index}>
                                    <td className="tg-za14">{otainfo.date}</td>
                                    <td className="tg-za14">{otainfo.shift}</td>
                                    <td className="tg-za14"> {otainfo.machine_Sl_No}</td>
                                    {/* <td className="tg-za14"> {otainfo.pressure_guage_value}</td> */}
                                    <td className="tg-za14"> {otainfo.checked_by}</td>
                                    <td className="tg-za14" colSpan="1">{otainfo.Otatime1}</td>
                                    <td className="tg-za14" colSpan="1">{otainfo.ota1}</td>
                                    <td className="tg-za14" colSpan="1">{otainfo.Otatime2}</td>
                                    <td className="tg-za14" colSpan="1">{otainfo.ota2} </td>
                                    <td className="tg-za14" colSpan="1">{otainfo.Otatime3}</td>
                                    <td className="tg-za14" colSpan="1"> {otainfo.ota3}</td>
                                    <td className="tg-za14" colSpan="1">{otainfo.Otatime4}</td>
                                    <td className="tg-za14" colSpan="1">{otainfo.ota4} </td>
                                    <td className="tg-za14" colSpan="1">{otainfo.Otatime5}</td>
                                    <td className="tg-za14" colSpan="1"> {otainfo.ota5}</td>
                                    <td className="tg-za14" colSpan="1">{otainfo.Otatime6}</td>
                                    <td className="tg-7zrl" colSpan="1">{otainfo.ota6} </td>
                                    <td className="tg-za14" colSpan="1">{otainfo.Otatime7}</td>
                                    <td className="tg-7zrl" colSpan="1">{otainfo.ota7} </td>
                                    <td className="tg-za14" colSpan="1">{otainfo.Otatime8}</td>
                                    <td className="tg-7zrl" colSpan="1">{otainfo.ota8} </td>
                                    <td className="tg-za14" colSpan="1">{otainfo.Otatime9}</td>
                                    <td className="tg-7zrl" colSpan="1">{otainfo.ota9} </td>
                                    <td className="tg-za14" colSpan="1">{otainfo.Otatime10}</td>
                                    <td className="tg-7zrl" colSpan="1">{otainfo.ota10} </td>
                                    <td className="tg-7zrl">{otainfo.status}</td>
                                </tr>
                            )) : <div>Please Wait</div>
                            }

                        </tbody>
                    </Table>
                    {
                        currentPosts.length === 0 ? <div className="text-center">No data</div> : null
                    }
                    {
                        ota.length >=15 ? <Paginatnation currentPage={currentPage} postsPerPage={postsPerPage}
                            totalPosts={ota.length}
                            paginate={this.paginate} /> : <div></div>
                    }
                </div>
            </>
        )
    }
}

