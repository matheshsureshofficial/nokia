import axios from 'axios';
import React, { Component } from 'react';
import { Navbar } from '../Navbar/Navbar'
import SweetAlert from "sweetalert2";
class Machine extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tablename: null,
            serialnumber: "",
        }
    }
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    machinesubmitbtn = async () => {
        const { tablename, serialnumber } = this.state
        if (tablename === null) return alert("Machine Table Name Required..")
        if (serialnumber.trim().length === 0) return alert("Machine Serial Number Required..")
        var machinedata = {
            table_name: tablename,
            serial_no: serialnumber
        }
        const machinedatacheck = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/viewall`).then((res) => {
            return res.data
        })
        if (serialnumber !== "none") {
            const filtermachine = await machinedatacheck.filter((machine, index) => { return machine.serial_no === serialnumber })
            console.log(filtermachine);
            if (filtermachine.length === 0) {
                var finalmachinedata = await axios.post(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/add`, machinedata).then((res) => { return res.data })
                if (finalmachinedata.length !== 0) {
                    SweetAlert.fire({
                        icon: 'success',
                        title: 'Machine Record Created Successfully',
                        confirmButtonText: `Save`,
                    }).then((res) => {
                        this.setState({
                            serialnumber: "",
                        })
                    })

                }
            } else {
                SweetAlert.fire({
                    icon: 'error',
                    title: 'Machine Serial Number Already Available..',
                    confirmButtonText: `Save`,
                }).then((res) => {
                    this.setState({
                        serialnumber: "",
                    })
                })

            }
        }

    }
    render() {

        return (
            <>
                <h3 className="text-center">Create Machine Serial Number</h3>
                <div className="row h-100vh">
                    <div className="col-md-4"></div>
                    <div className="col-md-4 mt-5">
                        <h6 >Table Name</h6>
                        <select type="text" name="tablename" className='form-control mt-3' placeholder="Enter Machine Table Name" onChange={e => this.handleChange(e)} >
                            <option >Choose Table Name</option>
                            <option value="UWA">UWA</option>
                            <option value="OTA">OTA</option>
                            <option value="PVA">PVA</option>
                            <option value="Soldering">Soldering</option>
                            <option value="Vaccume">Vaccume</option>
                        </select>
                        <h6 className="mt-5">Machine Serial Number</h6>
                        <input type="text" name="serialnumber" className='form-control ' value={this.state.serialnumber} placeholder="Enter Machine ID" onChange={e => this.handleChange(e)} />
                        <button className='mt-5' onClick={this.machinesubmitbtn}>Submit</button>
                    </div>
                    <div className="col-md-1"></div>
                    <div className="col-md-3 mt-5">
                    </div>
                </div>

            </>
        );
    }
}

export default Machine;