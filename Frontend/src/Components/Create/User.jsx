import axios from 'axios';
import React, { Component } from 'react';
import { Navbar } from '../Navbar/Navbar'
import SweetAlert from "sweetalert2";
class User extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: null,
            useremail: null,
            userpassword: null
        }

    }
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    usersubmitbtn = async () => {
        const { username, useremail, userpassword } = this.state
        console.log(username, useremail, userpassword);
        var userdata = {
            name: username,
            email: useremail,
            password: userpassword,
            role:"Tester"
        }
        var finaluserdata = await axios.post(`${process.env.REACT_APP_SERVER_ORIGIN}/user/register`, userdata).then((res) => { return res.data })
        if (finaluserdata.length !== 0) {
            SweetAlert.fire({
                icon: 'success',
                title: 'User Record Created Successfully',
                confirmButtonText: `Save`,
            })
            setTimeout(() => {
                window.location.reload()
            }, 2000)
        }
    }
    render() {
        return (
            <>
                <Navbar logo="NOKIA" subTitle="Digital WorkStation" title="Create User Details" />
                <div className="row h-100vh">
                    <div className="col-md-4"></div>
                    <div className="col-md-4 mt-5">
                        <h6 className="mt-5">User Name</h6>
                        <input type="text" name="username" className='form-control ' placeholder="Enter User Name" onChange={e => this.handleChange(e)} />
                        <h6 className="mt-5">User Email</h6>
                        <input type="text" name="useremail" className='form-control ' placeholder="Enter User Email" onChange={e => this.handleChange(e)} />
                        <h6 className="mt-5">User Password</h6>
                        <input type="text" name="userpassword" className='form-control ' placeholder="Enter User Password" onChange={e => this.handleChange(e)} />
                        <button className='mt-5' onClick={this.usersubmitbtn}>Submit</button>
                    </div>
                    <div className="col-md-1"></div>
                    <div className="col-md-3 mt-5">
                    </div>
                </div>

            </>
        );
    }
}

export default User;