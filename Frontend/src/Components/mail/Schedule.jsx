import React, { useState, useEffect } from 'react'
import moment from "moment"
import axios from 'axios'
export default function Schedule() {
    const [todayTime, setnewTime] = useState(null)
    const [mailData, setmailData] = useState([])
    useEffect(() => {
        const timeNow = moment().format("HH:mm:ss")
        setTimeout(() => { setnewTime(timeNow) }, 1000)
    }, [todayTime])

    var shiftStart = ["08:45:00", "13:45:00", "18:45:00", "13:01:20"]
    const Shift = async () => {
        const hours = moment().format("HH:mm")
        if (hours < "14:30") {
            return "Shift A"
        } else if (hours > "14.30" && hours < "22.30") {
            return "Shift B"
        } else {
            return "Shift C"
        }
    }
    const notEntrymachines = async (machineData, machinenos) => {
        if (machineData.length === 0) return { inactive: machinenos, active: [] }
        var notEntrymachine = [], active = []
        for (var i = 0; i < machinenos.length; i++) {
            var machinedata = await machineData.filter((data) => { return data.machine_Sl_No === machinenos[i] })
            if (machinedata.length === 0) {
                notEntrymachine.push(machinenos[i])
            } else {
                active.push(machinenos[i])
            }
        }
        return { inactive: notEntrymachine, active: active }
    }
    const convertString = (machinenos) => {
        var finalmachinenos = ""
        for (var j = 0; j < machinenos.length; j++) {
            if (j === 0) {
                finalmachinenos = `${machinenos[j]},`
            } else {
                finalmachinenos += `${machinenos[j]},`
            }
        }
        return finalmachinenos
    }
    const sendMail = async () => {
        var todayDate = moment().format("YYYY-MM-DD")
        var shift = await Shift()
        const vaccume = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/vaccume`).then((res) => { return res.data })
        const soldering = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/soldering`).then((res) => { return res.data })
        const ota = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/ota`).then((res) => { return res.data })
        const uwa = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/uwa`).then((res) => { return res.data })
        const pva = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/pva`).then((res) => { return res.data })
        const todayVaccumeData = await vaccume.filter((data) => { return data.date === todayDate && data.shift === shift })
        const todaysolderingData = await soldering.filter((data) => { return data.date === todayDate && data.shift === shift })
        const todayotaData = await ota.filter((data) => { return data.date === todayDate && data.shift === shift })
        const todayuwaData = await uwa.filter((data) => { return data.date === todayDate && data.shift === shift })
        const todaypvaData = await pva.filter((data) => { return data.date === todayDate && data.shift === shift })
        const vaccumemachineno = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/vaccume`).then((res) => { return res.data })
        const solderingmachineno = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/soldering`).then((res) => { return res.data })
        const otamachineno = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/ota`).then((res) => { return res.data })
        const uwamachineno = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/uwa`).then((res) => { return res.data })
        const pvamachineno = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/pva`).then((res) => { return res.data })
        const vaccumenotEntrymachines = await notEntrymachines(todayVaccumeData, vaccumemachineno)
        const solderingnotEntrymachines = await notEntrymachines(todaysolderingData, solderingmachineno)
        const otanotEntrymachines = await notEntrymachines(todayotaData, otamachineno)
        const uwanotEntrymachines = await notEntrymachines(todayuwaData, uwamachineno)
        const pvanotEntrymachines = await notEntrymachines(todaypvaData, pvamachineno)
        const vaccumestringinactive = await convertString(vaccumenotEntrymachines.inactive)
        const vaccumestringactive = await convertString(vaccumenotEntrymachines.active)
        const solderingstringinactive = await convertString(solderingnotEntrymachines.inactive)
        const solderingstringactive = await convertString(solderingnotEntrymachines.active)
        const otastringinactive = await convertString(otanotEntrymachines.inactive)
        const otastringactive = await convertString(otanotEntrymachines.active)
        const uwastringinactive = await convertString(uwanotEntrymachines.inactive)
        const uwastringactive = await convertString(uwanotEntrymachines.active)
        const pvastringinactive = await convertString(pvanotEntrymachines.inactive)
        const pvastringactive = await convertString(pvanotEntrymachines.active)
        const mailData = [
            {
                inactivemachineNo: vaccumestringinactive.length === 0 ? "Nill" : vaccumestringinactive,
                activemachineNo: vaccumestringactive.length === 0 ? "Nill" : vaccumestringactive,
                shift: shift,
                table: "Vaccume"
            },
            {
                inactivemachineNo: solderingstringinactive.length === 0 ? "Nill" : solderingstringinactive,
                activemachineNo: solderingstringactive.length === 0 ? "Nill" : solderingstringactive,
                shift: shift,
                table: "Soldering"
            },
            {
                inactivemachineNo: otastringinactive.length === 0 ? "Nill" : otastringinactive,
                activemachineNo: otastringactive.length === 0 ? "Nill" : otastringactive,
                shift: shift,
                table: "OTA"
            },
            {
                inactivemachineNo: uwastringinactive.length === 0 ? "Nill" : uwastringinactive,
                activemachineNo: uwastringactive.length === 0 ? "Nill" : uwastringactive,
                shift: shift,
                table: "UWA"
            },
            {
                inactivemachineNo: pvastringinactive.length === 0 ? "Nill" : pvastringinactive,
                activemachineNo: pvastringactive.length === 0 ? "Nill" : pvastringactive,
                shift: shift,
                table: "PVA"
            }
        ]
        return setmailData(mailData)

    }

    if (todayTime !== null) {
        if (shiftStart.includes(todayTime)) {
            sendMail()
            var mailtemplate = document.getElementById("mailtemplate").innerHTML
            console.log(mailtemplate);
            axios.post(`${process.env.REACT_APP_SERVER_ORIGIN}/mail/schedule`, {
                mailtemplate: mailtemplate
            }).then((res) => { return res.data })

        }
    }

    return (
        <div id='mailtemplate' style={{ display: "none" }}>
            <table>
                <thead>
                    <tr>
                        <th>Machine Name</th>
                        <th>Active Machine No</th>
                        <th>In-Active Machine No</th>
                        <th>Shift</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        mailData.length !== 0 ? mailData.map((data, index) => (
                            <tr key={index}>
                                <td>{data.table}</td>
                                <td>{data.activemachineNo}</td>
                                <td>{data.inactivemachineNo}</td>
                                <td>{data.shift}</td>
                            </tr>
                        )) : <tr>
                            <td>Nill</td>
                            <td>Nill</td>
                            <td>Nill</td>
                            <td>Nill</td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    )
}
