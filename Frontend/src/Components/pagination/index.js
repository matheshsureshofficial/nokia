import React from 'react'

export default function Index({ postsPerPage, totalPosts, paginate,currentPage }) {
    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
        pageNumbers.push(i);
    }
    return (
        <React.Fragment>
            <nav>
                <ul className='pagination'>
                    {pageNumbers.map(number => (
                        <li key={number} className={`page-item ${currentPage===number ? "active" : ""}`} style={{cursor:"pointer"}}>
                            <span onClick={() => paginate(number)}  className='page-link'>
                                {number}
                            </span>
                        </li>
                    ))}
                </ul>
            </nav>
        </React.Fragment>

    );
}
