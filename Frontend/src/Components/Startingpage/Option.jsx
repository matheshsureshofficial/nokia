import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Navbar } from '../Navbar/Navbar'
import list from '../../assets/img/list.png'
import "./Option.css"
import "animate.css"
import dashboardIcon from "../../assets/img/home/app-store.svg";
class Option extends Component {
    render() {
        return (
            <>
                <Navbar logo="NOKIA" NavMenu='show' subTitle="Digital  WorkStation" title="Autonomous Maintenance" src={dashboardIcon} alt="" />
                <div className="bg-blue" style={{height:"70vh",marginTop:"200px"}}>
                    <div className="row animate__animated animate__zoomIn">
                        <div className="col">
                            <div className="container">
                            <center>
                            <div className="card box-shadow" style={{width:"30vw"}}>
                                <div className="card-header">
                                <h4>Select Your Option To Get In</h4>    
                                </div>
                                <div className="card-body">
                                    <Link className="text-white" to="/scanner"><button className="btn-curve disabled"  style={{width: "-webkit-fill-available"}} disabled>Continue With QR Scanner</button></Link><br/><br/>
                                    <Link className="text-white" to="/home"><button className="btn-curve" style={{width: "-webkit-fill-available"}}>Continue With Manual</button></Link>
                                </div>
                            </div>
                            </center>
                            </div>
                        </div>
                        <div className="col my-auto d-flex flex-column align-content-center align-items-center justify-content-center">
                        <div className="col-xxl-7">
                            <div className="container">
                                <h4 style={{ textAlign: "center", lineHeight: "1.6" }}>
                                    Autonomous maintenance Check points for Key Process Stages can be accessed by operators.
                                    The Check lists need to be filled and maintained at beginning of every shift to ensure
                                    process quality and efficiency of these equipment.
                                </h4>
                                </div>
                                <center><img src={list} alt="" style={{ height: "200px" }} /></center>
                                <Link to='/Dashboard'><h4 style={{ textAlign: "center", color: "#124191", fontWeight: "600" }}>Click Here to Go Reports</h4></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Option;