import axios from 'axios'
import React, { Component } from 'react'
import { Redirect } from 'react-router'
import Scanner from './Scanner'
import Scanner2 from './Scanner2'

export default class Index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            scanner: 0,
            scanmachineid: null,
            scanclientid: null,
            isredirect: false,
            redirecturl: null,
            data: null,
            paths: [
                {
                    table_name: "UWA",
                    path: "/uwaform"
                },
                {
                    table_name: "OTA",
                    path: "/otaform"
                },
                {
                    table_name: "PVA",
                    path: "/Pvaform"
                },
                {
                    table_name: "Soldering",
                    path: "/SolderForm"
                }, {
                    table_name: "Vaccume",
                    path: "/VacuumForm"
                }
            ]
        }
    }
    nextbtnfun = () => {
        this.setState({
            scanner: 1
        })
    }
    subbtnfun = async () => {
        const { scanmachineid, scanclientid, paths } = this.state
        console.log(scanmachineid, scanclientid)
        var userdetails = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/user/${scanclientid}`).then((res) => { return res.data })
        var machinedetails = await axios.patch(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/${scanmachineid}`).then((res) => { return res.data })
        console.log(machinedetails, userdetails)
        for (var i = 0; i < paths.length; i++) {
            console.log(paths[i].table_name)
            if (paths[i].table_name === machinedetails[0].table_name) {
                this.setState({
                    redirecturl: paths[i].path,
                    data: {
                        name: userdetails[0].name,
                        machineid: machinedetails[0].serial_no
                    },
                    isredirect: true
                })
            }
        }

    }
    scanid = async (id, from) => {
        console.log(id, from)
        if (from === "userid") {
            this.setState({
                scanclientid: id
            })
        } else {
            this.setState({
                scanmachineid: id
            })
        }
    }
    render() {
        const { scanner, isredirect, redirecturl, data } = this.state
        if (isredirect === true) {
            return <Redirect to={{
                pathname: redirecturl,
                state: data
            }} />
        }

        return (
            <div>
                {
                    scanner === 0 ? <Scanner nextbtnfun={this.nextbtnfun} scanid={this.scanid} /> : <Scanner2 subbtnfun={this.subbtnfun} scanid={this.scanid} />
                }
            </div>
        )
    }
}
