import React, { Component } from 'react';
import QrReader from 'react-qr-scanner'
import { Link } from 'react-router-dom';
import { Navbar } from '../Navbar/Navbar'
import axios from 'axios'

class Scanner2 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            delay: 100,
            resultmachine: null,
            resultuser: null,
            operationname: null,
            operatorname: null,
            ismanual: false
        }
    }

    handleScan = async (data) => {
        const { scanid } = this.props
        if (data !== null) {
            this.setState({
                resultuser: data.text,
            })
            scanid(data.text, "userid")
            var userdetails = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/user/${data.text}`).then((res) => { return res.data })
            if (userdetails.length !== 0) {
                this.setState({
                    operatorname: userdetails[0].name
                })
            }

        }
    }
    manualbtn = () => {
        this.setState({
            ismanual: !this.state.ismanual
        })
    }
    handleChange = (e) => {
        this.setState({
            resultuser: e.target.value
        })
    }
    manualsubmitbtn = async () => {
        const { resultuser } = this.state
        const { scanid } = this.props
        scanid(resultuser, "userid")
        var userdetails = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/user/${resultuser}`).then((res) => { return res.data })
        if (userdetails.length !== 0) {
            this.setState({
                operatorname: userdetails[0].name
            })
        }else{
            alert("In-valid User ID")
        }
    }
    handleError = (err) => {
        console.error(err)
    }
    render() {
        const previewStyle = {
            height: 420,
            width: 530,
        }
        const { resultuser, operatorname, ismanual } = this.state
        const { subbtnfun } = this.props

        return (
            <>
                <Navbar logo="NOKIA" subTitle="Digital WorkStation" title="QR Code Scanner" />
                <div className="row bg-blue">


                    <div className="col-md-4"></div>
                    <div className="col-md-4">
                        <h4 className="text-center mt-5 mb-5">User QR Code Scanner</h4>

                        {
                            !ismanual ? <QrReader
                                delay={this.state.delay}
                                style={previewStyle}
                                onError={this.handleError}
                                onScan={this.handleScan}
                            /> : <div>
                                <input type="text" className='form-control' placeholder="Enter User ID" onChange={e => this.handleChange(e)} />
                                <button className='mt-3' onClick={this.manualsubmitbtn}>Submit</button>
                            </div>
                        }
                    </div>
                    <div className="col-md-1"></div>
                    <div className="col-md-3 mt-5">
                        <div className="mt-5">
                            <h4>User Id : {resultuser}</h4>
                            <h4>User Name : {operatorname}</h4>
                        </div>
                        <div className="mt-5">
                            <button onClick={this.manualbtn} >{!ismanual ? "Enter Manually" : "Scan QR Code" }</button>
                        </div>
                    </div>

                </div>
                <div className="row mt-5">
                    <div className="col-md-10"></div>
                    <div className="col-md-2">
                        {resultuser !== null ? <button onClick={subbtnfun} >Next</button> : null}
                    </div>
                </div>
            </>
        );
    }
}

export default Scanner2;