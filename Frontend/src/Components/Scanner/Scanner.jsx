import React, { Component } from 'react';
import QrReader from 'react-qr-scanner'
import { Link } from 'react-router-dom';
import { Navbar } from '../Navbar/Navbar'
import axios from 'axios'

class Scanner extends Component {
    constructor(props) {
        super(props)
        this.state = {
            delay: 100,
            resultmachine: null,
            operationname: null,
            ismanual: false
        }
    }

    handleScan = async (data) => {
        const { scanid } = this.props
        if (data !== null) {
            this.setState({
                resultmachine: data.text,
            })
            scanid(data.text, "machineid")
            var machinedetails = await axios.patch(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/${data.text}`).then((res) => { return res.data })
            if (machinedetails.length !== 0) {
                this.setState({
                    operationname: machinedetails[0].table_name
                })
            }
        }

    }
    handleError = (err) => {
        console.error(err)
    }
    manualbtn = () => {
        this.setState({
            ismanual: !this.state.ismanual
        })
    }
    handleChange = (e) => {
        this.setState({
            resultmachine: e.target.value
        })
    }
    manualsubmitbtn = async () => {
        const { resultmachine } = this.state
        const { scanid } = this.props
        scanid(resultmachine, "machineid")
        var machinedetails = await axios.patch(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/${resultmachine}`).then((res) => { return res.data })
        if (machinedetails.length !== 0) {
            this.setState({
                operationname: machinedetails[0].table_name
            })
        }else{
            alert("In-valid Machine ID")
        }
    }
    render() {
        const previewStyle = {
            height: 420,
            width: 530,
        }
        const { resultmachine, operationname, ismanual } = this.state
        // if (resultmachine !== null) {
        //     alert("Machine Qr scan Successfully..")
        // }
        const { nextbtnfun } = this.props

        return (
            <>
                <Navbar logo="NOKIA" subTitle="Digital WorkStation" title="QR Code Scanner" />
                <div className="row bg-blue" style={{height: "90vh"}}>


                    <div className="col-md-4"></div>
                    <div className="col-md-4">
                        <h4 className="text-center mt-5 mb-5">Machine QR Code Scanner</h4>
                        {
                            !ismanual ? <QrReader
                                delay={this.state.delay}
                                style={previewStyle}
                                onError={this.handleError}
                                onScan={this.handleScan}
                            /> : <div>
                                <input type="text" className='form-control' placeholder="Enter Machine ID" onChange={e => this.handleChange(e)} /><br/>
                                <button className='mt-3' onClick={this.manualsubmitbtn}>Submit</button>
                            </div>
                        }


                    </div>
                    <div className="col-md-1"></div>
                    <div className="col-md-3 mt-5">
                        <div className="mt-5">
                            <h4>Machine Si.No : {resultmachine}</h4>
                            <h4>Operation : {operationname}</h4>
                        </div>
                        <div className="mt-5">
                            <button onClick={this.manualbtn} >{!ismanual ? "Enter Manually" : "Scan QR Code"}</button>
                        </div>

                    </div>

                </div>
                <div className="row mt-5">
                    <div className="col-md-10"></div>
                    <div className="col-md-2">
                        {resultmachine !== null ? <button onClick={nextbtnfun}>Next</button> : null}
                    </div>
                </div>
            </>
        );
    }
}

export default Scanner;