import React, { Component } from 'react'
import { Form, OverlayTrigger, Tooltip } from 'react-bootstrap'
import { Redirect } from 'react-router'
import { connect } from 'react-redux'
import formImg from '../../../assets/img/formImg.gif'
import SubmitButton from '../../Utilities/Buttons/SubmitButton'
import { Navbar } from '../../Navbar/Navbar'
import { uwaActions } from '../../../Reducers/UwaReducer'
import axios from 'axios'
import moment from 'moment'
export class Testerform extends Component {
    constructor(props) {
        super()
        this.state = {
            shift: null,
            date: null,
            Station: '',
            stationError: false,
            nameError: false,
            operator_name: '',
            istesterform: false
        }
    }

    componentDidMount = () => {
        if (this.props !== undefined) {
            if (this.props.location !== undefined) {
                if (this.props.location.state !== undefined) {
                    this.setState({
                        operator_name: this.props.location.state.name,
                        Station: this.props.location.state.machineid
                    })
                }
            }
        }
        this.props.resetUwaState();
        var today = new Date();
        const hours = today.getHours() + ":" + today.getMinutes()
        const monthsandday = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
            "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60"]
        var date = today.getFullYear() + '-' + (monthsandday[today.getMonth() + 1]) + '-' + (monthsandday[today.getDate()]);
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;
        if (hours < "14:00") {
            this.setState({
                shift: "Shift A",
                date: dateTime
            })
        } else if (hours > "14:00" && hours < "22:00") {
            this.setState({
                shift: "Shift B",
                date: dateTime
            })
        } else {
            this.setState({
                shift: "Shift C",
                date: dateTime
            })
        }
    }

    handlechange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }


    submitbtn = async () => {
        localStorage.setItem("stationId", this.state.Station)
        localStorage.setItem("testerName", this.state.operator_name)
        const { Station, operator_name } = this.state
        // const machinedatacheck = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/viewall`).then((res) => {
        //     return res.data
        // })
        // if (Station !== "none") {
        //     const filtermachine = await machinedatacheck.filter((machine, index) => { return machine.serial_no === Station && machine.table_name === "UWA" })
        //     if (filtermachine.length !== 0) {
                if (Station.trim() === '') {
                    this.setState({ stationError: true })
                } else if (operator_name.trim() === '') {
                    this.setState({ nameError: true, stationError: false })
                } else {
                    const sendmail = await this.sendMail()
                    this.setState({ istesterform: true })
                    // this.props.history.push("/Testers");
                }
        //     } else {
        //         alert("Machine Serial Number Invalid..Please Check")
        //     }
        // }

    }
    notEntrymachines = async (machineData, machinenos) => {
        var notEntrymachine = []
        for (var i = 0; i < machinenos.length; i++) {
            var machinedata = await machineData.filter((data) => { return data.machine_Sl_No === machinenos[i] })
            if (machinedata.length === 0) {
                notEntrymachine.push(machinenos[i])
            }
        }
        return notEntrymachine
    }
    convertString = (machinenos) => {
        var finalmachinenos = ""
        for (var j = 0; j < machinenos.length; j++) {
            if (j === 0) {
                finalmachinenos = `${machinenos[j]},`
            } else {
                finalmachinenos += `${machinenos[j]},`
            }
        }
        return finalmachinenos
    }
    sendMail = async () => {
        var todayDate = moment().format("YYYY-MM-DD")
        const uwa = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/uwa`).then((res) => { return res.data })
        const todayuwaData = await uwa.filter((data) => { return data.date === todayDate })
        const uwamachineno = await axios.get(`${process.env.REACT_APP_SERVER_ORIGIN}/machineno/uwa`).then((res) => { return res.data })
        const uwanotEntrymachines = await this.notEntrymachines(todayuwaData, uwamachineno)
        const uwastring = await this.convertString(uwanotEntrymachines)

        if (uwastring.length !== 0) {
            const machinestatus = await axios.post(`${process.env.REACT_APP_SERVER_ORIGIN}/mail/machinestatus`, {
                machineNo: uwastring,
                table: "UWA",
                shift: this.state.shift
            }).then((res) => { return res.data })
            return machinestatus
        }
    }
    render() {
        const { istesterform } = this.state
        if (istesterform === true) {
            return <Redirect to={
                {
                    pathname: "/UWA",
                    state: this.state
                }
            } />
        }
        //ToolTips 
        const serialTooltip = (props) => (
            <Tooltip id="button-tooltip" {...props}>
                Enter Serial No
            </Tooltip>
        );

        const nameTooltip = (props) => (
            <Tooltip id="button-tooltip" {...props}>
                Enter Your Name
            </Tooltip>
        );
        const { shift, date, nameError, stationError } = this.state
        const stationClass = stationError ? 'w-50 text-uppercase border border-danger error-bg' : 'w-50 text-uppercase';
        const nameClass = nameError ? 'w-50 text-uppercase border border-danger error-bg' : 'w-50 text-uppercase';
        return (
            <>
                <Navbar logo="NOKIA" subTitle="Digital WorkStation" title="UWA Autonomous Maintenance" />

                <div className=" bg-primary d-flex justify-content-center flex-column align-items-center" style={{ height: "90vh", width: "100%" }}>
                    <div data-aos="zoom-in" data-aos-duration='1000' className='d-flex justify-content-center h-75 w-75'>
                        <div className='h-100 w-50 '>
                            <img src={formImg} alt='formImg' className='h-100 w-100' />
                        </div>
                        <div className='d-flex flex-column justify-content-center align-items-center h-100 w-50 glassCard bg-light' style={{ borderRadius: "0px" }}>
                            <h3 className='form-title'>UWA Autonomous Maintenance</h3>
                            <div>
                                <Form >
                                    <Form.Group className='d-sm-flex'>
                                        <Form.Label className=' input-label w-50 pt-1'>Date and Time</Form.Label>
                                        <Form.Control type="text" className='w-50 ' name="date" disabled defaultValue={date} />
                                    </Form.Group>
                                    <OverlayTrigger
                                        placement="right"
                                        delay={{ show: 250, hide: 400 }}
                                        overlay={serialTooltip}
                                    >
                                        <div>
                                            <Form.Group controlId="exampleForm.ControlSelect1" className=' d-sm-flex'>
                                                <Form.Label className=' input-label w-50 input-label pt-1'> Station Serial No / Scan</Form.Label>
                                                <Form.Control type='text' className={stationClass} name="Station" value={this.state.Station} onChange={(e) => this.handlechange(e)}>
                                                </Form.Control>
                                            </Form.Group>
                                            {this.state.stationError && <p className='errorMessage'>*The station serial no must not be empty</p>}
                                        </div>
                                    </OverlayTrigger>
                                    <OverlayTrigger
                                        placement="right"
                                        delay={{ show: 250, hide: 400 }}
                                        overlay={nameTooltip}
                                    >
                                        <div>
                                            <Form.Group className=' d-sm-flex'>
                                                <Form.Label className=' input-label w-50 pt-1'>Operator Name</Form.Label>
                                                <Form.Control type="text" className={nameClass} name="operator_name" value={this.state.operator_name} onChange={(e) => this.handlechange(e)} />
                                            </Form.Group>
                                            {this.state.nameError && <p className='errorMessage'>*The operator name must not be empty</p>}
                                        </div>
                                    </OverlayTrigger>

                                    <Form.Group controlId="exampleForm.ControlSelect11" className=' d-sm-flex'>
                                        <Form.Label className='input-label w-50 pt-1'>Shift</Form.Label>
                                        <Form.Control type="text" className='w-50 ' disabled name="shift" defaultValue={shift !== null ? shift : ""} />
                                    </Form.Group>
                                    <Form.Group className="d-flex justify-content-center mt-4">
                                        <div><SubmitButton onClick={this.submitbtn} buttonName="Submit" /></div>
                                    </Form.Group>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        uwaState: state.UwaStates
    }
}
const mapDispatchToProps = dispatch => {
    return {
        resetUwaState: () => dispatch(uwaActions.step_reset)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Testerform)

