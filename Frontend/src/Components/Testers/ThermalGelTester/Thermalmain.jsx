import React, { Component } from 'react'
import '../../../Pages/Styles/Pages.css'
import { Link } from 'react-router-dom'
import { Navbar } from '../../Navbar/Navbar'
import uwa1 from '../../../assets/Images/Tester/pva.png'
import uwa2 from '../../../assets/Images/Tester/asy.png'
import uwa3 from '../../../assets/Images/Tester/gel.png'

export default class Thermalmain extends Component {
    render() {
        return (
            <>
                {/* Navbar */}
                <Navbar logo="NOKIA" subTitle="Digital WorkStation" title="Thermal Gel Machine Autonomous Maintenance" />
                {/* Grid Menu Cards */}
                <div className="d-flex justify-content-center align-items-center bg-primary flex-column h-100vh">
                    <div className="d-flex justify-content-center my-5">
                        <Link to="/Pvaform" >
                            <div className='grid-card bg-card mr-5 white fa-2x d-flex justify-content-center flex-column align-items-center'>
                                <img className='w-100 h-100' src={uwa1} alt='asytmek' />
                            </div>
                        </Link>
                        {/* <Link to="/uwaform" > */}
                        <div className='grid-card bg-card mr-5 white fa-2x d-flex justify-content-center flex-column align-items-center'>
                            <img className='w-100 h-100' src={uwa2} alt='asytmek' />
                        </div>
                        {/* </Link> */}
                        {/* <Link to="/uwaform" > */}
                        <div className='grid-card bg-card mr-5 white fa-2x d-flex justify-content-center flex-column align-items-center'>
                            <img className='w-100 h-100' src={uwa3} alt='asytmek' />
                        </div>
                        {/* </Link> */}
                    </div>
                </div>
            </>
        )
    }
}
