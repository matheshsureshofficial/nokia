const router = require("express").Router()
const db = require("../database/mySql")

const {qrCode} =require("../helpers/qrcode")

router.post("/add",async (req, res) => {
    var newmachine_serial_no=req.body
    var machineqrcode=await qrCode(req.body.serial_no)
    newmachine_serial_no["qrcode"]=machineqrcode
    var sql = `INSERT INTO machine_serial_no SET ?`;
    db.query(sql,newmachine_serial_no, function (err, result) {
        if (err) {
            return res.send(err.message)
        } else {
            return res.send(newmachine_serial_no)
        }
    });
})

router.get("/viewall", (req, res) => {
    db.query("SELECT * FROM machine_serial_no", function (err, result, fields) {
        if (err) {
            return res.send(err)
        } else {
            return res.send(result)
        }
    });
})


router.get("/:tablename", async (req, res) => {
    const { tablename } = req.params
    const daterange11 = `SELECT * FROM machine_serial_no WHERE table_name=?`
    const tablenames = new Promise(async (resolve, reject) => {
        db.query(daterange11, [tablename], function (err, result, fields) {
            if (err) {
                return resolve(false)
            } else {
                return resolve(result)
            }
        });
    })
    const machinenos = await tablenames
    const machineNo = []
    for (var i = 0; i < machinenos.length; i++) {
        machineNo.push(machinenos[i].serial_no)
    }
    return res.send(machineNo)
})

router.patch("/:serial_no", async (req, res) => {
    const { serial_no } = req.params
    const daterange11 = `SELECT * FROM machine_serial_no WHERE serial_no=?`
    const tablenames = new Promise(async (resolve, reject) => {
        db.query(daterange11, [serial_no], function (err, result, fields) {
            if (err) {
                return resolve(false)
            } else {
                return resolve(result)
            }
        });
    })
    const machinenos = await tablenames
    
    return res.send(machinenos)
})


module.exports = router