const router = require("express").Router()
const email = require('emailjs')
const axios = require("axios")
const SMTPClient = email.SMTPClient


const client = new SMTPClient({
  //    host: '10.130.128.30',
  host: "mailrelay.int.nokia.com",
});

router.post("/", async (req, res) => {
  const { name, testing, failurestep, date, shift, table } = req.body
  var text = `
  Operator Name :${name}
  Tester ID :${testing}
  Date & Shifts :${date} & ${shift}
  Not Ok Parameters : ${failurestep},
  Standard :${table}

    `
  client.send(
    {
      text: text,
      from: "DWSAlerts@nokia.com",
      to: 'balachander.marimuthu@nokia.com',
      cc: 'agni.gnanamani@nokia.com',
      subject: "Notification for Solder Tip Monitoring",
    },
    (err, message) => {
      if (err) {
        console.log("mail Not Sent");
      } else {
        console.log("mail Sent Successfully");
        res.status(200).send("mail Sent Successfully")
      }
    }

  )
})
router.post("/schedule", async (req, res) => {
  const { mailtemplate } = req.body
  var text = mailtemplate
  client.send(
    {
      text: text,
      from: "DWSAlerts@nokia.com",
      to: 'balachander.marimuthu@nokia.com',
      cc: 'agni.gnanamani@nokia.com',
      subject: "Notification for Solder Tip Monitoring",
    },
    (err, message) => {
      if (err) {
        console.log("mail Not Sent");
        return res.send(false)
      } else {
        console.log("mail Sent Successfully");
        res.status(200).send("mail Sent Successfully")
        return res.send(true)
      }
    }
  )
})
router.post("/machinestatus", async (req, res) => {
  const { shift, machineNo, table } = req.body
  var text = `
  ${shift}
  ${machineNo}
  ${table}
  `
  client.send(
    {
      text: text,
      from: "DWSAlerts@nokia.com",
      to: 'balachander.marimuthu@nokia.com',
      cc: 'agni.gnanamani@nokia.com',
      subject: "Notification for Machine Status",
    },
    (err, message) => {
      if (err) {
        console.log("mail Not Sent");
        return res.send(false)
      } else {
        console.log("mail Sent Successfully");
        res.status(200).send("mail Sent Successfully")
        return res.send(true)
      }
    }
  )
})

const { sendMail } = require("../modules/schedule")

const moment = require("moment")
var shiftStart = ["08:45:00", "13:45:00", "18:45:00", "14:12:00"]
showTime();
async function showTime() {
  var time = moment().format("HH:mm:ss")
  if (shiftStart.includes(time)) {
    var sendmail = await sendMail()
    var mailtemplate = ""
    for (var i = 0; i < sendmail.length; i++) {
      mailtemplate +=
        `
          Machine Name:${sendmail[i].table}
          Shift:${sendmail[i].shift}
          Active Machine No:${sendmail[i].activemachineNo}
          In-Active Machine No:${sendmail[i].inactivemachineNo}
          `
    }
    client.send(
      {
        text: mailtemplate,
        from: "DWSAlerts@nokia.com",
        to: 'balachander.marimuthu@nokia.com',
        cc: 'agni.gnanamani@nokia.com',
        subject: "Notification for Solder Tip Monitoring",
      },
      (err, message) => {
        if (err) {
          console.log("mail Not Sent");
        } else {
          console.log("mail Sent Successfully");
          res.status(200).send("mail Sent Successfully")
        }
      }
    )
  }
  setTimeout(() => { showTime() }, 1000)
}

module.exports = router
