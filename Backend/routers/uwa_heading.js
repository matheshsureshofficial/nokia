const router = require("express").Router()
const db = require("../database/mySql")




router.post("/add", (req, res) => {
    const { heading, sub_heading } = req.body
    var sql = `INSERT INTO uwa_heading (heading,sub_heading) VALUES ('${heading}','${sub_heading}')`;
    db.query(sql, function (err, result) {
        if (err) {
            console.log(err)
            return res.send(false)
        } else {
            return res.send(true)
        }
    });
})


router.get("/viewall", (req, res) => {
    db.query("SELECT * FROM uwa_heading", function (err, result, fields) {
        if (err) {
            return res.send(err)
        } else {
            return res.send(result)
        }
    });
})


router.get("/:heading", (req, res) => {
    const { heading } = req.params
    const daterange11 = `SELECT * FROM uwa_heading WHERE heading=?`
    db.query(daterange11, [heading], function (err, result, fields) {
        if (err) {
            return res.send(false)
        } else {
            return res.send(result)
        }
    });
})

router.put("/:heading",async (req, res) => {
    const { heading } = req.params    
    const myquery = "UPDATE uwa_heading SET ? WHERE heading = ?"
    const updateheading = new Promise(async (resolve, reject) => {
        await db.query(myquery, [req.body, heading], (err, res) => {
            if (err) return resolve(err)
            return resolve(true)
        })
    })
    const update = await updateheading
    return res.send(update)
})
module.exports = router