-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2021 at 11:51 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nokia`
--

-- --------------------------------------------------------

--
-- Table structure for table `machine_serial_no`
--

CREATE TABLE `machine_serial_no` (
  `id` int(255) NOT NULL,
  `serial_no` varchar(256) NOT NULL,
  `table_name` varchar(256) NOT NULL,
  `qrcode` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `machine_serial_no`
--

INSERT INTO `machine_serial_no` (`id`, `serial_no`, `table_name`, `qrcode`) VALUES
(8, '1234567', 'UWA', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAK6SURBVO3BQa7jSAwFwUxC97/ym7/kSkBBsqdNMML8YY1RrFGKNUqxRinWKMUapVijFGuUYo1SrFGKNUqxRinWKMUapVijFGuUi4dUvikJnUqXhE6lS8IJlW9KwhPFGqVYoxRrlIuXJeFNKk8k4U1JeJPKm4o1SrFGKdYoFx+mciIJT6h0SfgklRNJ+KRijVKsUYo1ysWPS0Kn0qmcSMIvK9YoxRqlWKNc/DiVO0m4ozJJsUYp1ijFGuXiw5LwSUnoVDqVT0rCv6RYoxRrlGKNcvEylW9S6ZLQqXRJ6FROqPzLijVKsUYp1ijmD4OoPJGEX1asUYo1SrFGuXhIpUvCHZVPSkKn0iWhU+lUuiTcUemS0KmcSMITxRqlWKMUa5SLl6mcSEKn0iXhjkqn8kQSOpUuCXdUTiThTcUapVijFGuUi4eScEfljkqXhE7lThLuqNxJQqfyRBL+T8UapVijFGsU84cHVO4koVM5kYQTKl0SOpUuCW9SeSIJTxRrlGKNUqxRLh5KQqdyIgl3VLokdConknBCpUvCiSTcUXlTsUYp1ijFGsX84YepdEnoVE4k4Y5Kl4Q7KieS8ESxRinWKMUa5eIhlW9Kwh2VO0noVDqVEyonktCpvKlYoxRrlGKNcvGyJLxJ5UQS7qicSEKnciIJnconFWuUYo1SrFEuPkzlRBLepHIiCZ3KnSR0Kp1Kl4RO5U3FGqVYoxRrlIthVN6UhDsqTyThTcUapVijFGuUix+XhE6lS8ITKl0SuiR0KndU7iThiWKNUqxRijXKxYcl4ZuS0Kl0SbijckKlS0Kn0iWhU3lTsUYp1ijFGuXiZSrfpHInCZ1Kl4QuCSeScEKlS8KbijVKsUYp1ijmD2uMYo1SrFGKNUqxRinWKMUapVijFGuUYo1SrFGKNUqxRinWKMUapVij/AdY3P8G7YmO2gAAAABJRU5ErkJggg=='),
(9, '9876543', 'UWA', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAKnSURBVO3BQW7kQAwEwSxC//9yro88NSBI47VpRsQvrDGKNUqxRinWKMUapVijFGuUYo1SrFGKNUqxRinWKMUapVijFGuUYo1y8VASvpPKSRJOVO5IwndSeaJYoxRrlGKNcvEylTcl4Q6VkyScqJyovCkJbyrWKMUapVijXHxYEu5QuSMJ/1MS7lD5pGKNUqxRijXKxXAqf0mxRinWKMUa5eKPScKJym9WrFGKNUqxRrn4MJXvpNIloVPpkvCEyk9SrFGKNUqxRrl4WRImS8JPVqxRijVKsUa5eEjlJ1PpknCHym9SrFGKNUqxRrl4KAmdSpeEN6l0KidJ6FS6JJwk4U0qn1SsUYo1SrFGuXhZEjqVLgknKidJOFF5k0qXhE7ljiScqDxRrFGKNUqxRrn4z1ROktCpnCThJAmdSpeETqVT6ZLQqZyodEl4U7FGKdYoxRolfuFFSThR6ZLQqdyRhDtUnkjCiUqXhBOVNxVrlGKNUqxR4hceSEKn0iXhROUkCZ3KSRI6lS4JnUqXhE7lJAknKl0STlSeKNYoxRqlWKPEL/xiSThR6ZLwhMoTSThReaJYoxRrlGKNcvFQEr6TSqfSJeFEpUtCp9IloUtCp3KShO9UrFGKNUqxRrl4mcqbknCShJMk3JGEE5WTJHQqXRI6lTcVa5RijVKsUS4+LAl3qLxJ5SQJncpJEjqVTqVLQqfyScUapVijFGuUi2FU/rJijVKsUYo1ysUwSbhDpUtCp9KpdEnoVP6nYo1SrFGKNcrFh6l8ksqbVE6S8EQSOpU3FWuUYo1SrFEuXpaE75SEE5UuCZ1Kl4ROpVPpknCHyicVa5RijVKsUeIX1hjFGqVYoxRrlGKNUqxRijVKsUYp1ijFGqVYoxRrlGKNUqxRijVKsUb5B+f/B+uFOTCpAAAAAElFTkSuQmCC'),
(10, '45678', 'OTA', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAKvSURBVO3BQW7sWAwEwSxC979yjpdcPUCQ2r+HZkT8wRqjWKMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1SrFGKdYoFw8l4Tep3JGEE5WTJPwmlSeKNUqxRinWKBcvU3lTEp5Q6ZLQJaFTOVF5UxLeVKxRijVKsUa5+LAk3KFyRxLuUOmS8EQS7lD5pGKNUqxRijXKxTAqf1mxRinWKMUa5WK4JHQqkxVrlGKNUqxRLj5M5V9S6ZLQqTyh8k2KNUqxRinWKBcvS8I3SUKn0iWhUzlJwjcr1ijFGqVYo8Qf/I8l4Q6VyYo1SrFGKdYoFw8loVPpkvAmlU7ljiR0KidJeJPKJxVrlGKNUqxR4g9elIRO5Y4kdConSehUuiS8SeWbFWuUYo1SrFEuHkpCp3KShBOVLgmdSqdyh0qXhE6lS0KXhE6lS8KJSpeETuWJYo1SrFGKNUr8wQcl4Q6VkyR0Kl0SOpUuCU+onCShU/lNxRqlWKMUa5SLlyXhROWOJDyRhE6lS0KncpKETqVT6ZJwh8oTxRqlWKMUa5SLl6m8SeUkCZ1Kl4RvpvKmYo1SrFGKNcrFQ0n4TSqdSpeETqVLQqfSJaFT6VS6JHQq/1KxRinWKMUa5eJlKm9KwkkS7lA5UTlJwjcr1ijFGqVYo1x8WBLuUHlTEk5UuiR0Kp1Kl4Q7VD6pWKMUa5RijXLxxyXhJAmdSqfSJaFT+U3FGqVYoxRrlIs/RqVLQqfSJaFLQqfSqfxLxRqlWKMUa5SLD1P5JJU7knCShBOVkyR0Kl0SOpU3FWuUYo1SrFEuXpaE35SEE5VOpUtCp3KShE6lU+mS0Kl8UrFGKdYoxRol/mCNUaxRijVKsUYp1ijFGqVYoxRrlGKNUqxRijVKsUYp1ijFGqVYoxRrlP8A0/wT4gs7JykAAAAASUVORK5CYII='),
(11, '98234567', 'OTA', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAKuSURBVO3BQW7sWAwEwSxC979yjpdcPUCQusfmZ0T8wRqjWKMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1SrFGKdYoFw8l4ZtUTpLQqTyRhG9SeaJYoxRrlGKNcvEylTcl4ZOS0KmcqLwpCW8q1ijFGqVYo1x8WBLuULkjCZ3KSRLelIQ7VD6pWKMUa5RijXLxj1OZpFijFGuUYo1yMUwSOpWTJHQqf1mxRinWKMUa5eLDVL5JpUtCp9Il4QmV36RYoxRrlGKNcvGyJEyWhN+sWKMUa5RijXLxkMpvkoROpUvCHSp/SbFGKdYoxRrl4qEkdCpdEt6k0ql0SThR6ZJwkoQ3qXxSsUYp1ijFGuXiIZUTlS4JJyq/iUqXhE6lS8IdSehUnijWKMUapVijXLwsCZ3KE0m4Q+UkCZ1Kl4ROpVPpknBHEjqVNxVrlGKNUqxR4g8eSMIdKl0SOpWTJDyh8kQS3qTypmKNUqxRijVK/MEDSehUTpLQqXRJOFHpkvCESpeETuUkCZ3KSRJOVJ4o1ijFGqVYo8Qf/GFJ6FS6JLxJ5SQJT6g8UaxRijVKsUaJP3ggCd+kcpKETuUkCZ1Kl4QTld+kWKMUa5RijXLxMpU3JeEkCXck4SQJJyp3JKFT6ZLQqTxRrFGKNUqxRrn4sCTcofKESpeETqVLQqdykoROpUvCHSpvKtYoxRqlWKNcDJOETuWbVP5PxRqlWKMUa5SLYVS6JJyodEnoVDqVLgmdSpeETuWTijVKsUYp1igXH6bySSpdEjqVO1ROkvBEEjqVNxVrlGKNUqxRLl6WhG9KQqfSJeFEpUtCp9KpdEm4Q+WTijVKsUYp1ijxB2uMYo1SrFGKNUqxRinWKMUapVijFGuUYo1SrFGKNUqxRinWKMUapVij/AePbA7nc1IOXgAAAABJRU5ErkJggg=='),
(12, '987654', 'PVA', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAALHSURBVO3BQY7rWg4FwTyE9r/lbM+aowsIkv1fEYyIH6wxijVKsUYp1ijFGqVYoxRrlGKNUqxRijVKsUYp1ijFGqVYoxRrlGKNcvFQEn5J5SQJJyp3JOGXVJ4o1ijFGqVYo1y8TOVNSThJwh1J6FTuUHlTEt5UrFGKNUqxRrn4siTcoXKHSpeEX0rCHSrfVKxRijVKsUa5GEalS0Kn0iWhU/nLijVKsUYp1igXf1wS1v8Va5RijVKsUS6+TOWbVE6S8E0q/5JijVKsUYo1ysXLkvBLSehUvikJ/7JijVKsUYo1SvxgkCR0Kl0STlT+smKNUqxRijXKxUNJ6FS6JHQqXRI6lS4JncqbVJ5IQqdykoRO5U3FGqVYoxRrlPjBDyXhROWOJNyhckcSOpWTJJyodEnoVJ4o1ijFGqVYo8QPHkjCiUqXhF9SOUlCp3JHEjqV/1KxRinWKMUaJX7wQBI6lS4JJypPJOEOlTuS0KmcJKFT+aVijVKsUYo1ysU/LgmdSqdykoSTJHQqdyThjiR0Km8q1ijFGqVYo8QP/rAkdConSehUuiR0Kl0SOpUuCScqXRI6lSeKNUqxRinWKBcPJeGXVDqVLglvSsIdKl0STlTeVKxRijVKsUa5eJnKm5LwhEqXhC4JJypdEroknKj8UrFGKdYoxRrl4suScIfKHUl4QqVLQpeEE5WTJHQqXRI6lSeKNUqxRinWKBd/nEqXhC4JncqJSpeETuWJJHQqbyrWKMUapVijXPxxSehUuiScJKFT6VROktCpdConSehUnijWKMUapVijXHyZyjepdEnoVE5UTpLQqXQqJ0noVDqVNxVrlGKNUqxRLl6WhF9KwkkSOpUuCZ3KHUnoVO5IQqfyRLFGKdYoxRolfrDGKNYoxRqlWKMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1yv8ABPMb72wyFOsAAAAASUVORK5CYII='),
(13, 's4we56g', 'PVA', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAKzSURBVO3BQY6kAAwEwSyL/385t48+ISGgZ8briPjBGqNYoxRrlGKNUqxRijVKsUYp1ijFGqVYoxRrlGKNUqxRijVKsUYp1igHNyXhm1SuSEKnckUSvknljmKNUqxRijXKwcNUnpSEK5JwJglnVM6oPCkJTyrWKMUapVijHLwsCVeoXJGETqVLwpuScIXKm4o1SrFGKdYoB8OpdEnoVCYp1ijFGqVYoxwMl4ROpUtCp/KXFWuUYo1SrFEOXqbyTUk4k4RO5Q6V36RYoxRrlGKNcvCwJPwklS4JnUqXhE7lTBJ+s2KNUqxRijVK/OAPS0Kn8j8r1ijFGqVYoxzclIROpUvCk1Q6lS4JnUqXhE7lTBKepPKmYo1SrFGKNUr84EFJOKNyJgmdypkk/CSVLgl3qNxRrFGKNUqxRjm4KQlXJKFTOZOEO1TOJKFT6ZJwhUqXhE7lTcUapVijFGuU+MENSXiSypkkdCpdEt6k0iXhSSp3FGuUYo1SrFEOHqbSJeGOJHQqXRI6lS4JnUqXhE7lCpUuCZ3KNxVrlGKNUqxRDm5SOaNyh8odKk9KwhVJuELljmKNUqxRijVK/OCGJHyTyh1J6FS6JHQqVyShU+mS0Kk8qVijFGuUYo0SP7ghCZ3Kk5LQqXRJ6FTelIQ3qdxRrFGKNUqxRjl4WRKuUHlTEjqVLgmdSqdyRxI6lScVa5RijVKsUQ6GScIVSTiThE6lS8IVKm8q1ijFGqVYoxwMo9IloVPpktCpdEnoktCpdEn4ScUapVijFGuUg5epvEnljEqXhDNJOKNyRqVLwjcVa5RijVKsUQ4eloRvSkKn0iWhU+mS0KnckYROpUtCp/KkYo1SrFGKNUr8YI1RrFGKNUqxRinWKMUapVijFGuUYo1SrFGKNUqxRinWKMUapVijFGuUfzYyD++BDlOJAAAAAElFTkSuQmCC'),
(14, '23435fdg', 'Soldering', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAALASURBVO3BQY7cQAwEwSxC//9yeo88NSBIM17TjIg/WGMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1SrFGKdYoxRrl4qEkfJPKSRJOVO5IwjepPFGsUYo1SrFGuXiZypuScIfKJ6m8KQlvKtYoxRqlWKNcfFgS7lC5IwlPqDyRhDtUPqlYoxRrlGKNcjGMSpeE/0mxRinWKMUa5WKYJJyodEnoVP5lxRqlWKMUa5SLD1P5JpWTJHQqT6j8JsUapVijFGuUi5cl4TdJQqfSJaFTOUnCb1asUYo1SrFGiT/4hyWhU/mfFWuUYo1SrFEuHkpCp9Il4U0qnUqXhE6lS0KncpKEN6l8UrFGKdYoxRrl4iGVLgmdykkSOpWTJHQqncodSXhCpUvCHUnoVJ4o1ijFGqVYo1y8TOUkCXckoVPpktCpdEnoVLokdCpdEu5Q6ZLwTcUapVijFGuUi79MpUtCp3Ki0iXhJAknSbgjCb9JsUYp1ijFGuXiZUnoVDqVLgmdSpeETqVLwonKSRI6lTuS0KmcJOGTijVKsUYp1igXL1O5Q+VE5Q6VLgmdyhNJ6FTuUPmkYo1SrFGKNUr8wQNJ+CaVJ5LQqXRJ6FROktCpdEk4UXlTsUYp1ijFGuXiZSpvSsJJEjqVLgmdyonKSRI6ld+kWKMUa5RijXLxYUm4Q+VNKl0SOpUuCZ3KSRI6lROVTyrWKMUapVijXAyThE7lJAknSThR6ZLwNxVrlGKNUqxRLoZLQqfSJaFT6ZLQqXRJ6FT+pmKNUqxRijXKxYepfJJKl4ROpUvCSRJOktCpdEnoVLokdCpvKtYoxRqlWKNcvCwJ35SEO1S6JHQqdyShU+mS0Kl8UrFGKdYoxRol/mCNUaxRijVKsUYp1ijFGqVYoxRrlGKNUqxRijVKsUYp1ijFGqVYoxRrlD9jhhX4TG5nVwAAAABJRU5ErkJggg=='),
(15, 'hfh654vx', 'Soldering', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAKsSURBVO3BQY7cQAwEwSxC//9yeo88NSBIM17TjIg/WGMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1SrFGKdYoxRrl4qEkfJNKl4QTlSeS8E0qTxRrlGKNUqxRLl6m8qYknKh0SbgjCZ3KicqbkvCmYo1SrFGKNcrFhyXhDpU7ktCpdEn4pCTcofJJxRqlWKMUa5SLf5zKHSpdEiYp1ijFGqVYo1wMk4RO5X9SrFGKNUqxRrn4MJVPSkKncofKEyq/SbFGKdYoxRrl4mVJ+E2S0Kl0SehUTpLwmxVrlGKNUqxR4g8GScIdKpMUa5RijVKsUS4eSkKn0iWhU7kjCScqTyShUzlJQqdykoROpUtCp/JEsUYp1ijFGuXiZUk4SUKn0iWhU7lD5U1JOEnCico3FWuUYo1SrFEuPkzlJAmdSpeETqVLwh0qJ0k4UemS8JsUa5RijVKsUS4+LAknKicqT6icJKFT+SaVNxVrlGKNUqxRLh5S6ZLQqdyRhCdUuiR0Kp1Kl4QTlSeS0Km8qVijFGuUYo1y8TKVJ1TuSMIdSehU7kjCiUqXhE6lS0Kn8kSxRinWKMUa5eKhJHyTykkSOpUuCV0STlROktAl4W8q1ijFGqVYo8QfPJCETuVNSehU7kjCiUqXhBOV36xYoxRrlGKNcvFhSbhD5ZuS8EQSOpWTJHQqbyrWKMUapVijXAyThE7lTUk4SUKn0ql8UrFGKdYoxRrl4h+XhJMkdCp3JOFNSThReaJYoxRrlGKNcvFhKp+k8kQSOpVOpUtCp3KHyicVa5RijVKsUS5eloRvSsKJyonKSRI6lS4JncodSehUnijWKMUapVijxB+sMYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5RijfIHdlQA/h6Dhd0AAAAASUVORK5CYII='),
(16, '42gd55', 'Vaccume', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAK6SURBVO3BQW7sWAwEwSxC979yzl9y9QBB6rbNYUT8hzVGsUYp1ijFGqVYoxRrlGKNUqxRijVKsUYp1ijFGqVYoxRrlGKNUqxRLh5KwjepdEk4UXkiCd+k8kSxRinWKMUa5eJlKm9KwhNJOFG5Q+VNSXhTsUYp1ijFGuXiw5Jwh8odKl0STlTelIQ7VD6pWKMUa5RijXLxxyWhU+mScJKETuUvK9YoxRqlWKNc/HEqT6hMUqxRijVKsUa5+DCVb0rCN6n8JsUapVijFGuUi5cl4ZuS0Kl0SThJQqdykoTfrFijFGuUYo0S/+EPS0Kn8n9WrFGKNUqxRrl4KAmdSpeEE5UuCXeodEk4UbkjCZ3KSRI6lS4JJypPFGuUYo1SrFEuHlLpknCi0iXhROUkCZ1Kl4STJHQqnUqXhE6lU+mS8E3FGqVYoxRrlIuHknCi0iXhRKVLQqdyh8odSbgjCXeodEl4U7FGKdYoxRrl4sOScKLSJaFT6ZLQqZwkoVO5Q+WOJHQqXRI6lTcVa5RijVKsUS5+WBJOktCpnCShU+mS0Kl0SbgjCZ3KTyrWKMUapVijXDyk8pOScIfKm1R+k2KNUqxRijXKxUNJ+CaVb0pCp9Il4Tcp1ijFGqVYo1y8TOVNSbhD5SQJb1LpknCi0iWhU3miWKMUa5RijXLxYUm4Q+UOlS4Jd6icJKFLQqdyRxI+qVijFGuUYo1y8ccloVPpktCpnCShU+mScIdKl4RO5U3FGqVYoxRrlIvhVLokdCpvUumS0Kl8UrFGKdYoxRrl4sNUPkmlS0KncpKETqVLwh1J6FROktCpPFGsUYo1SrFGuXhZEr4pCZ+k0iWhS8JJEjqVTuVNxRqlWKMUa5T4D2uMYo1SrFGKNUqxRinWKMUapVijFGuUYo1SrFGKNUqxRinWKMUapVij/AdoLQMOlm3IXQAAAABJRU5ErkJggg=='),
(17, 'ggfd455454', 'Vaccume', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAKlSURBVO3BQW7sWAwEwSxC979yjpdcPUCQusefZkT8wRqjWKMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1SrFGKdYoFw8l4ZtUTpLQqXRJ6FROkvBNKk8Ua5RijVKsUS5epvKmJNyhckcSOpUTlTcl4U3FGqVYoxRrlIsPS8IdKnckoVPpknCi8kQS7lD5pGKNUqxRijXKxR+j0iWhU/mXFWuUYo1SrFEuhklCp/KXFGuUYo1SrFEuPkzlN0lCp/KEym9SrFGKNUqxRrl4WRL+TypdEjqVLgmdykkSfrNijVKsUYo1SvzBPywJJyp/SbFGKdYoxRrl4qEkdCpdEt6k0ql0SThJQqdykoQ3qXxSsUYp1ijFGuXiIZU7VN6UhE7ljiR0Kp3KSRKeSEKn8kSxRinWKMUaJf7gi5LQqXRJOFHpknCHSpeEE5U7ktCpdEnoVN5UrFGKNUqxRok/+KAkdCpdEk5UnkjCHSpdEjqVLglvUnmiWKMUa5RijRJ/8EFJ6FROkvCEyh1J6FQ+KQmdypuKNUqxRinWKBcfpnKHyh1J+KYkdCpdEk5UuiR0Kk8Ua5RijVKsUeIPHkjCN6mcJOEOlSeS0KmcJKFTeVOxRinWKMUa5eJlKm9KwkkSTlQ+SaVLwonKJxVrlGKNUqxRLj4sCXeovCkJJyp3JOFE5SQJncqbijVKsUYp1igXw6icJKFLwh0qJ0k4UfmkYo1SrFGKNcrFH6PSJaFT6ZLQJaFT6VS6JHxTsUYp1ijFGuXiw1Q+SaVLwhNJ+JcVa5RijVKsUS5eloRvSsIdKl0SOpWTJJwk4SQJncqbijVKsUYp1ijxB2uMYo1SrFGKNUqxRinWKMUapVijFGuUYo1SrFGKNUqxRinWKMUapVij/AeKtgPpr3Kb2AAAAABJRU5ErkJggg=='),
(18, 'fgsrt565', 'Soldering', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAKTSURBVO3BQY7cQAwEwSxC//9yeo88NSBIM/bSjIg/WGMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1SrFGKdYoxRrl4qEkfJNKl4ROpUtCp9IloVPpkvBNKk8Ua5RijVKsUS5epvKmJNyRhE7lTSpvSsKbijVKsUYp1igXH5aEO1R+kyTcofJJxRqlWKMUa5SLX06lS8L/rFijFGuUYo1y8csloVP5nxVrlGKNUqxRLj5M5ZNUuiR8k8q/pFijFGuUYo1y8bIkfFMSOpUuCZ1Kl4Q7kvAvK9YoxRqlWKPEHwyShCdUfrNijVKsUYo1ysVDSehUTpLwSSp3JKFLQqdykoROpUvCHSpPFGuUYo1SrFEuHlL5JJWTJJwkoVO5IwknKl0STlQ+qVijFGuUYo1y8VASnlA5SUKn0qk8odIl4QmVkyScqDxRrFGKNUqxRok/eFESOpUuCScqJ0noVE6ScKLySUnoVLokdCpPFGuUYo1SrFEuvkylS0KXhBOVLgl3qJwk4UTlDpVvKtYoxRqlWKNcvEzlDpU3qZwk4UTlTUk4UXlTsUYp1ijFGuXioSR8k8oTKl0SuiS8SeWbijVKsUYp1igXL1N5UxLelIQ7VE6ScJKEO1SeKNYoxRqlWKNcfFgS7lD5m1S6JJyo3JGETyrWKMUapVijXAyThDepdEnoknCHyicVa5RijVKsUS5+OZUuCZ3KE0l4QqVLwonKE8UapVijFGuUiw9T+SaVLgmdykkSTlROktAl4ZuKNUqxRinWKBcvS8I3JeFEpUtCp9Kp3JGEE5VvKtYoxRqlWKPEH6wxijVKsUYp1ijFGqVYoxRrlGKNUqxRijVKsUYp1ijFGqVYoxRrlGKN8gdIeevvrSINFQAAAABJRU5ErkJggg==');

-- --------------------------------------------------------

--
-- Table structure for table `otatable`
--

CREATE TABLE `otatable` (
  `id` int(255) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `machine_Sl_No` varchar(255) DEFAULT NULL,
  `shift` varchar(255) DEFAULT NULL,
  `checked_by` varchar(255) DEFAULT NULL,
  `ota1` varchar(255) DEFAULT NULL,
  `ota2` varchar(255) DEFAULT NULL,
  `ota3` varchar(255) DEFAULT NULL,
  `ota4` varchar(255) DEFAULT NULL,
  `ota5` varchar(255) DEFAULT NULL,
  `ota6` varchar(255) DEFAULT NULL,
  `ota7` varchar(255) DEFAULT NULL,
  `ota8` varchar(255) DEFAULT NULL,
  `ota9` varchar(255) DEFAULT NULL,
  `ota10` varchar(255) DEFAULT NULL,
  `Otatime1` varchar(255) DEFAULT NULL,
  `Otatime2` varchar(255) DEFAULT NULL,
  `Otatime3` varchar(255) DEFAULT NULL,
  `Otatime4` varchar(255) DEFAULT NULL,
  `Otatime5` varchar(255) DEFAULT NULL,
  `Otatime6` varchar(255) DEFAULT NULL,
  `Otatime7` varchar(255) DEFAULT NULL,
  `Otatime8` varchar(255) DEFAULT NULL,
  `Otatime9` varchar(255) DEFAULT NULL,
  `Otatime10` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `average` varchar(255) DEFAULT NULL,
  `statuslists` varchar(255) DEFAULT NULL,
  `applieddate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `otatable`
--

INSERT INTO `otatable` (`id`, `date`, `machine_Sl_No`, `shift`, `checked_by`, `ota1`, `ota2`, `ota3`, `ota4`, `ota5`, `ota6`, `ota7`, `ota8`, `ota9`, `ota10`, `Otatime1`, `Otatime2`, `Otatime3`, `Otatime4`, `Otatime5`, `Otatime6`, `Otatime7`, `Otatime8`, `Otatime9`, `Otatime10`, `description`, `status`, `average`, `statuslists`, `applieddate`) VALUES
(1, '2021-09-13', '45678', 'Shift A', 'kalidas', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', '2', '2', '1', '2', '1', '0', '0', '0', '1', '1', 'undefined', 'In Complete', '8/10', 'Ota2,Ota4', '2021-09-15 06:12:04'),
(2, '2021-09-15', '45678', 'Shift B', 'kalidas', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', '2', '2', '1', '2', '1', '0', '0', '0', '1', '1', 'undefined', 'In Complete', '8/10', 'Ota2,Ota4', '2021-09-15 06:11:22'),
(3, '2021-09-15', '45678', 'Shift A', 'kalidas', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', '2', '2', '1', '2', '1', '0', '0', '0', '1', '1', 'undefined', 'In Complete', '8/10', 'Ota2,Ota4', '2021-09-15 06:11:22'),
(4, '2021-09-15', '45678', 'Shift B', 'kalidas', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', '2', '2', '1', '2', '1', '0', '0', '0', '1', '1', 'undefined', 'In Complete', '8/10', 'Ota2,Ota4', '2021-09-15 06:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `ota_heading`
--

CREATE TABLE `ota_heading` (
  `id` int(255) NOT NULL,
  `heading` varchar(256) NOT NULL,
  `sub_heading` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pvatable`
--

CREATE TABLE `pvatable` (
  `id` int(255) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `machine_Sl_No` varchar(255) DEFAULT NULL,
  `shift` varchar(255) DEFAULT NULL,
  `checked_by` varchar(255) DEFAULT NULL,
  `pressure_guage_value` varchar(255) DEFAULT NULL,
  `pva1` varchar(255) DEFAULT NULL,
  `pva2` varchar(255) DEFAULT NULL,
  `pva3` varchar(255) DEFAULT NULL,
  `pva4` varchar(255) DEFAULT NULL,
  `pva5` varchar(255) DEFAULT NULL,
  `pva6` varchar(255) DEFAULT NULL,
  `pva7` varchar(255) DEFAULT NULL,
  `pva8` varchar(255) DEFAULT NULL,
  `pva9` varchar(255) DEFAULT NULL,
  `pva10` varchar(255) DEFAULT NULL,
  `pva11` varchar(255) DEFAULT NULL,
  `pva12` varchar(255) DEFAULT NULL,
  `pva13` varchar(255) DEFAULT NULL,
  `pvatime1` varchar(255) DEFAULT NULL,
  `pvatime2` varchar(255) DEFAULT NULL,
  `pvatime3` varchar(255) DEFAULT NULL,
  `pvatime4` varchar(255) DEFAULT NULL,
  `pvatime5` varchar(255) DEFAULT NULL,
  `pvatime6` varchar(255) DEFAULT NULL,
  `pvatime7` varchar(255) DEFAULT NULL,
  `pvatime8` varchar(255) DEFAULT NULL,
  `pvatime9` varchar(255) DEFAULT NULL,
  `pvatime10` varchar(255) DEFAULT NULL,
  `pvatime11` varchar(255) DEFAULT NULL,
  `pvatime12` varchar(255) DEFAULT NULL,
  `pvatime13` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `average` varchar(255) DEFAULT NULL,
  `statuslists` varchar(255) DEFAULT NULL,
  `applieddate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pvatable`
--

INSERT INTO `pvatable` (`id`, `date`, `machine_Sl_No`, `shift`, `checked_by`, `pressure_guage_value`, `pva1`, `pva2`, `pva3`, `pva4`, `pva5`, `pva6`, `pva7`, `pva8`, `pva9`, `pva10`, `pva11`, `pva12`, `pva13`, `pvatime1`, `pvatime2`, `pvatime3`, `pvatime4`, `pvatime5`, `pvatime6`, `pvatime7`, `pvatime8`, `pvatime9`, `pvatime10`, `pvatime11`, `pvatime12`, `pvatime13`, `description`, `status`, `average`, `statuslists`, `applieddate`) VALUES
(1, '2021-09-15', 's4we56g', 'Shift A', 'kalidas', '150', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', '8', '7', '7', '6', '7', '6', '7', '9', '9', '6', '6', '7', '6', 'Not Provided', 'In Complete', '7/10', 'pva3,pva6,pva12', '2021-09-15 06:27:11'),
(2, '2021-09-13', 's4we56g', 'Shift B', 'kalidas', '150', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', '8', '7', '7', '6', '7', '6', '7', '9', '9', '6', '6', '7', '6', 'Not Provided', 'In Complete', '7/10', 'pva3,pva6,pva12', '2021-09-15 06:27:11'),
(3, '2021-09-15', 's4we56g', 'Shift A', 'kalidas', '150', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', '8', '7', '7', '6', '7', '6', '7', '9', '9', '6', '6', '7', '6', 'Not Provided', 'In Complete', '7/10', 'pva3,pva6,pva12', '2021-09-15 06:27:11'),
(4, '2021-09-13', 's4we56g', 'Shift B', 'kalidas', '150', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'Yes', 'Yes', 'Yes', 'No', 'Yes', '8', '7', '7', '6', '7', '6', '7', '9', '9', '6', '6', '7', '6', 'Not Provided', 'In Complete', '7/10', 'pva3,pva6,pva12', '2021-09-15 06:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `pva_heading`
--

CREATE TABLE `pva_heading` (
  `id` int(255) NOT NULL,
  `heading` varchar(256) NOT NULL,
  `sub_heading` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `solderingtable`
--

CREATE TABLE `solderingtable` (
  `id` int(255) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `shift` varchar(255) DEFAULT NULL,
  `defaultTemp` varchar(256) NOT NULL,
  `machine_Sl_No` varchar(255) DEFAULT NULL,
  `station` varchar(255) DEFAULT NULL,
  `catridge_used` varchar(255) DEFAULT NULL,
  `temperature` varchar(255) DEFAULT NULL,
  `checked_by` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `applieddate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `replacecount` int(255) NOT NULL,
  `remeasurecount` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `solderingtable`
--

INSERT INTO `solderingtable` (`id`, `date`, `time`, `shift`, `defaultTemp`, `machine_Sl_No`, `station`, `catridge_used`, `temperature`, `checked_by`, `description`, `status`, `applieddate`, `replacecount`, `remeasurecount`) VALUES
(2, '2021-09-15', '3', 'Shift B', '393', '12344', '12344', 'STTC 198 / 393°c', '393', 'dk', 'Not Provided', 'Complete', '2021-09-15 10:29:12', 1, 0),
(3, '2021-09-15', '12', 'Shift B', '425', '12344', '12344', 'STTC 804L / 425°c', '418', 'rajesh', 'Not Provided', 'Complete', '2021-09-15 11:04:36', 6, 5),
(4, '2021-09-16', '27', 'Shift A', '425', '23435fdg', '23435fdg', 'STTC 804L / 425°c', '426', 'kalidas', 'Not Provided', 'Complete', '2021-09-16 07:59:52', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `thermaltable`
--

CREATE TABLE `thermaltable` (
  `id` int(255) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `machine_Sl_No` varchar(255) DEFAULT NULL,
  `shift` varchar(255) DEFAULT NULL,
  `checked_by` varchar(255) DEFAULT NULL,
  `thermal1` varchar(255) DEFAULT NULL,
  `thermal2` varchar(255) DEFAULT NULL,
  `thermal3` varchar(255) DEFAULT NULL,
  `thermal4` varchar(255) DEFAULT NULL,
  `thermal5` varchar(255) DEFAULT NULL,
  `thermal6` varchar(255) DEFAULT NULL,
  `thermal7` varchar(255) DEFAULT NULL,
  `thermal8` varchar(255) DEFAULT NULL,
  `thermal9` varchar(255) DEFAULT NULL,
  `thermal10` varchar(255) DEFAULT NULL,
  `thermal11` varchar(255) DEFAULT NULL,
  `thermal12` varchar(255) DEFAULT NULL,
  `thermal13` varchar(255) DEFAULT NULL,
  `thermaltime1` varchar(255) DEFAULT NULL,
  `thermaltime2` varchar(255) DEFAULT NULL,
  `thermaltime3` varchar(255) DEFAULT NULL,
  `thermaltime4` varchar(255) DEFAULT NULL,
  `thermaltime5` varchar(255) DEFAULT NULL,
  `thermaltime6` varchar(255) DEFAULT NULL,
  `thermaltime7` varchar(255) DEFAULT NULL,
  `thermaltime8` varchar(255) DEFAULT NULL,
  `thermaltime9` varchar(255) DEFAULT NULL,
  `thermaltime10` varchar(255) DEFAULT NULL,
  `thermaltime11` varchar(255) DEFAULT NULL,
  `thermaltime12` varchar(255) DEFAULT NULL,
  `thermaltime13` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `average` varchar(255) DEFAULT NULL,
  `statuslists` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `userid` varchar(256) NOT NULL,
  `token` varchar(256) NOT NULL,
  `role` varchar(256) NOT NULL,
  `qrcode` longtext NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `userid`, `token`, `role`, `qrcode`, `create_date`) VALUES
(1, 'john', 'john@gmail.com', '$2a$10$F0GN71Cs8LKD1iSb/jHbQe8RL8WVLwZpvJyDKSUgcZ8iRrdAx4Cji', '1630994232437', 'eyJhbGciOiJIUzI1NiJ9.MTYzMDk5NDIzMjQzNw.QGkAyh2uzSryiyx5lTTXane7YBziyds6pauCKCYccho', 'Tester', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAK/SURBVO3BQY7DVgwFwX6E7n/ljpdcfUCQ7GQYVsUP1hjFGqVYoxRrlGKNUqxRijVKsUYp1ijFGqVYoxRrlGKNUqxRijVKsUa5eCgJv6TyS0n4JZUnijVKsUYp1igXL1N5UxJOknCi0iWhU7lD5U1JeFOxRinWKMUa5eLLknCHyh0qXRJOVN6UhDtUvqlYoxRrlGKNcvE/l4RO5S8r1ijFGqVYo1z8cUm4IwmTFWuUYo1SrFEuvkzlm1S6JHRJOFF5QuW/pFijFGuUYo1y8bIk/FISOpUuCZ1Kl4RO5SQJ/2XFGqVYoxRrlPjBIEk4UZmsWKMUa5RijXLxUBI6lS4JnUqXhE6lS0KncofKSRI6lTuS0KmcJKFTeVOxRinWKMUaJX7wQBI6lZMk3KHSJaFTOUnCiUqXhE6lS0Kn0iWhUzlJQqfyRLFGKdYoxRrl4suS0Kl0SehUuiR0KidJeFMSnkhCp/JNxRqlWKMUa5T4wQNJ6FROknCHSpeEX1LpkvBNKk8Ua5RijVKsUeIHL0pCp/JEEk5UTpJwotIl4Q6VkyR0Kt9UrFGKNUqxRrl4mcqbVE6S0KmcqJyodEl4UxI6lTcVa5RijVKsUeIHDyThl1TelIRO5Y4kvEnliWKNUqxRijXKxctU3pSEO5JwotKpnCThRKVLwolKl4Q3FWuUYo1SrFEuviwJd6jckYRO5Y4kdCpPqPybijVKsUYp1igXf5xKl4Q7VLokdCpvSkKn8qZijVKsUYo1ysUfl4RO5SQJdyThRKVLQqfSqXRJ6FSeKNYoxRqlWKNcfJnKN6mcJOEkCScqXRLepPKmYo1SrFGKNcrFy5LwS0noVDqVLgmdSpeELgnflIRO5YlijVKsUYo1SvxgjVGsUYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5R/AKMMFe/r2odfAAAAAElFTkSuQmCC', '2021-09-07 05:57:12'),
(2, 'sam', 'sam@gmail.com', '$2a$10$i0VbPHQ2jTNE34qwrvt7r.OgjXA4tWxoxVaJjG0KtIBhqR02rdJfC', '1630994245817', 'eyJhbGciOiJIUzI1NiJ9.MTYzMDk5NDI0NTgxNw.xlHON0QqdjxD3gkFmEF7eQ5tByjc8OEDEkFA3qlregM', 'Tester', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAKmSURBVO3BQW7sWAwEwSxC979yjpdcPaAhyb/NYUT8wRqjWKMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1SrFGKdYoFzcl4TepnCShU+mS0KmcJOE3qdxRrFGKNUqxRrl4mMqTkvAJlS4JJ0noVE5UnpSEJxVrlGKNUqxRLl6WhE+ofCIJd6jckYRPqLypWKMUa5RijXIxjMpJEiYr1ijFGqVYo1z8zyWhU/nLijVKsUYp1igXL1P5l5LQqXRJuEPlmxRrlGKNUqxRLh6WhMmS8M2KNUqxRinWKBc3qXyTJHQqXRI+ofKXFGuUYo1SrFEubkpCp9Il4UkqncpJEjqVLgknSXiSypuKNUqxRinWKBc3qXRJOFF5UhI6lSepdEnoVE6S0CXhROWOYo1SrFGKNcrFl0tCp3KShE6lS0Kn0iWhU+lUuiR8QuVNxRqlWKMUa5T4gxcloVM5SUKnckcSOpU7knCi8okkdCp3FGuUYo1SrFEubkrCiUqXhBOVLgmdykkSOpUuCZ1Kl4ROpVPpknCShE7lTcUapVijFGuU+IM/LAn/kkqXhE7lNxVrlGKNUqxRLm5Kwm9S6VTuSEKn0iXhjiScqDypWKMUa5RijXLxMJUnJeEkCU9Kwh0qJ0noktCp3FGsUYo1SrFGuXhZEj6hcodKl4ROpUtCp/KJJHQqJypvKtYoxRqlWKNcDJOETuVNKt+kWKMUa5RijXIxjEqXhBOVLgmdyh1J6FTeVKxRijVKsUa5eJnKm1S6JJyonKg8SaVLQqfypGKNUqxRijXKxcOS8JuS0KmcJKFT6ZLQqXRJ6FS6JJyovKlYoxRrlGKNEn+wxijWKMUapVijFGuUYo1SrFGKNUqxRinWKMUapVijFGuUYo1SrFGKNcp/drID9QSbhXgAAAAASUVORK5CYII=', '2021-09-07 05:57:25'),
(3, 'jack', 'jack@gmail.com', '$2a$10$vT.UCfLl24jaSbduGOSWiu8YgtMM43Z63.Xs0qRo5Hv74G69tL9tu', '1630994309603', 'eyJhbGciOiJIUzI1NiJ9.MTYzMDk5NDMwOTYwMw.777u-G1_CFGzwhZdoMDYKDIJw2CbUIep5uJ-8qlP7fQ', 'Tester', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAKsSURBVO3BQa7jSAwFwXyE7n/lHC+5KkCQ7OlPMCJ+sMYo1ijFGqVYoxRrlGKNUqxRijVKsUYp1ijFGqVYoxRrlGKNUqxRijXKxUNJ+CWVLglvUumS8EsqTxRrlGKNUqxRLl6m8qYk3KHSJeFE5Q6VNyXhTcUapVijFGuUiy9Lwh0qb1LpkvCmJNyh8k3FGqVYoxRrlIs/TqVLwonKZMUapVijFGuUiz8uCU8koVP5y4o1SrFGKdYoF1+m8k0qXRI6lS4Jb1L5lxRrlGKNUqxRLl6WhF9KQqfSJaFT6ZJwRxL+ZcUapVijFGuU+MFgSbhD5S8r1ijFGqVYo1w8lIRO5SQJv6RykoQuCZ3KSRI6lS4Jd6g8UaxRijVKsUa5eEilS8KJykkSOpWTJHQqXRI6lROVLgmdyptU3lSsUYo1SrFGuXgoCXck4USlS8ITKicqXRK+SaVLQqfyRLFGKdYoxRolfvCiJHQqXRJOVE6S8CaVNyWhUzlJQqfyRLFGKdYoxRolfvBAEn5J5Y4kdCp3JKFTOUnCHSpvKtYoxRqlWKPED/6wJJyodEk4UXkiCZ1Kl4QTlSeKNUqxRinWKPGDB5LwSypdEjqVO5LwSyrfVKxRijVKsUa5eJnKm5JwRxI6lS4Jd6icJKFTOUlCp/KmYo1SrFGKNcrFlyXhDpX/k0qXhE7lJAmdSqfSJaFTeaJYoxRrlGKNcrGOVO5QuUPlTcUapVijFGuUiz9OpUvCicodSbgjCZ1Kl4QTlSeKNUqxRinWKBdfpvJLKl0SOpWTJJyonCThRKVLwpuKNUqxRinWKBcvS8IvJeGOJHQqncpJEp5IQqfypmKNUqxRijVK/GCNUaxRijVKsUYp1ijFGqVYoxRrlGKNUqxRijVKsUYp1ijFGqVYoxRrlP8A7FMM40Ettn8AAAAASUVORK5CYII=', '2021-09-07 05:58:29'),
(4, 'test', 'test@gmail.com', '$2a$10$wJDVhUMjN0QQTRhmZmwNhuuBw8E8cgEOD6hhDkdySu4M2.obT2SMa', '1631518017452', 'eyJhbGciOiJIUzI1NiJ9.MTYzMTUxODAxNzQ1Mg.PEpUg_CcGU7pOR43KTjbMAKIr4bVHCf6yT4efJMbCW4', 'Tester', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAK3SURBVO3BQW7sWAwEwSxC979yjpdcPUCQur/NYUT8wRqjWKMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1SrFGKdYoFw8l4ZtUTpLQqXRJ6FROkvBNKk8Ua5RijVKsUS5epvKmJNyh0iWhU3lC5U1JeFOxRinWKMUa5eLDknCHyh1JOFE5SUKnckcS7lD5pGKNUqxRijXKxf9MEjqVSYo1SrFGKdYoF8Oo3JGETuUvK9YoxRqlWKNcfJjKNyWhU+lUuiQ8ofKbFGuUYo1SrFEuXpaEf0mlS0Kn8kQSfrNijVKsUYo1SvzBH5aEN6n8ZcUapVijFGuUi4eS0Kl0SXiTSqdykoROpUvCSRLepPJJxRqlWKMUa5SLL1PpknCi0iWhUzlReUKlS0KncpKELgknKk8Ua5RijVKsUeIPXpSETuUkCXeodEk4UemS0Kl0SehUTpLQqfxLxRqlWKMUa5T4gxcl4Q6VO5LQqXRJ6FTelIQnVLokdCpPFGuUYo1SrFEuPkzlJAmdSpeEkyTckYROpUtCp9KpdEn4TYo1SrFGKdYo8Qd/WBI6lS4Jn6TSJaFT+aZijVKsUYo1ysVDSfgmlU7lROUkCZ1Kl4Q3JaFTeVOxRinWKMUa5eJlKm9KwkkSTlS6JJwk4QmVLgknSehUnijWKMUapVijXHxYEu5QeULlRKVLQqdykoQuCZ1Kp/JNxRqlWKMUa5SL4ZLQqbxJ5SQJnconFWuUYo1SrFEuhklCp9IloVPpktCpdConSfiXijVKsUYp1igXH6bySSpdEk5UTlROknCicpKETuVNxRqlWKMUa5SLlyXhm5JwkoQTlS4JncodSehUOpVPKtYoxRqlWKPEH6wxijVKsUYp1ijFGqVYoxRrlGKNUqxRijVKsUYp1ijFGqVYoxRrlGKN8h+B/RfkpFh8swAAAABJRU5ErkJggg==', '2021-09-13 07:26:57');

-- --------------------------------------------------------

--
-- Table structure for table `uwatable`
--

CREATE TABLE `uwatable` (
  `id` int(255) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `machine_Sl_No` varchar(255) DEFAULT NULL,
  `shift` varchar(255) DEFAULT NULL,
  `checked_by` varchar(255) DEFAULT NULL,
  `uwa1` varchar(255) DEFAULT NULL,
  `uwa2` varchar(255) DEFAULT NULL,
  `uwa3` varchar(255) DEFAULT NULL,
  `uwa4` varchar(255) DEFAULT NULL,
  `uwa5` varchar(255) DEFAULT NULL,
  `uwa6` varchar(255) DEFAULT NULL,
  `uwa7` varchar(255) DEFAULT NULL,
  `uwa8` varchar(255) DEFAULT NULL,
  `uwa9` varchar(255) DEFAULT NULL,
  `uwa10` varchar(255) DEFAULT NULL,
  `uwatime1` varchar(255) DEFAULT NULL,
  `uwatime2` varchar(255) DEFAULT NULL,
  `uwatime3` varchar(255) DEFAULT NULL,
  `uwatime4` varchar(255) DEFAULT NULL,
  `uwatime5` varchar(255) DEFAULT NULL,
  `uwatime6` varchar(255) DEFAULT NULL,
  `uwatime7` varchar(255) DEFAULT NULL,
  `uwatime8` varchar(255) DEFAULT NULL,
  `uwatime9` varchar(255) DEFAULT NULL,
  `uwatime10` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `average` varchar(255) DEFAULT NULL,
  `statuslists` varchar(255) DEFAULT NULL,
  `applieddate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `uwatable`
--

INSERT INTO `uwatable` (`id`, `date`, `machine_Sl_No`, `shift`, `checked_by`, `uwa1`, `uwa2`, `uwa3`, `uwa4`, `uwa5`, `uwa6`, `uwa7`, `uwa8`, `uwa9`, `uwa10`, `uwatime1`, `uwatime2`, `uwatime3`, `uwatime4`, `uwatime5`, `uwatime6`, `uwatime7`, `uwatime8`, `uwatime9`, `uwatime10`, `description`, `status`, `average`, `statuslists`, `applieddate`) VALUES
(1, '2021-09-15', '9876543', 'Shift A', 'kalidas', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', '1', '1', '1', '0', '0', '1', '0', '1', '0', '0', 'undefined', 'In Complete', '7/10', 'Uwa3,Uwa6,Uwa9', '2021-09-15 06:21:21'),
(2, '2021-09-13', '9876543', 'Shift B', 'kalidas', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', '1', '1', '1', '0', '0', '1', '0', '1', '0', '0', 'undefined', 'In Complete', '7/10', 'Uwa3,Uwa6,Uwa9', '2021-09-15 06:21:21'),
(3, '2021-09-15', '9876543', 'Shift A', 'kalidas', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', '1', '1', '1', '0', '0', '1', '0', '1', '0', '0', 'undefined', 'In Complete', '7/10', 'Uwa3,Uwa6,Uwa9', '2021-09-15 06:21:21'),
(4, '2021-09-15', '9876543', 'Shift B', 'kalidas', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', 'Yes', 'No', 'Yes', '1', '1', '1', '0', '0', '1', '0', '1', '0', '0', 'undefined', 'In Complete', '7/10', 'Uwa3,Uwa6,Uwa9', '2021-09-15 06:21:21');

-- --------------------------------------------------------

--
-- Table structure for table `uwa_heading`
--

CREATE TABLE `uwa_heading` (
  `id` int(255) NOT NULL,
  `heading` varchar(256) NOT NULL,
  `sub_heading` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vaccumetable`
--

CREATE TABLE `vaccumetable` (
  `id` int(255) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `shift` varchar(255) DEFAULT NULL,
  `machine_Sl_No` varchar(255) DEFAULT NULL,
  `checked_by` varchar(255) DEFAULT NULL,
  `pressure_guage_value` varchar(255) DEFAULT NULL,
  `process1_result` varchar(255) DEFAULT NULL,
  `process2_result` varchar(255) DEFAULT NULL,
  `process3_result` varchar(255) DEFAULT NULL,
  `process4_result` varchar(255) DEFAULT NULL,
  `process5_result` varchar(255) DEFAULT NULL,
  `process6_result` varchar(255) DEFAULT NULL,
  `process7_result` varchar(255) DEFAULT NULL,
  `process8_result` varchar(255) DEFAULT NULL,
  `process9_result` varchar(255) DEFAULT NULL,
  `process1_time` varchar(255) DEFAULT NULL,
  `process2_time` varchar(255) DEFAULT NULL,
  `process3_time` varchar(255) DEFAULT NULL,
  `process4_time` varchar(255) DEFAULT NULL,
  `process5_time` varchar(255) DEFAULT NULL,
  `process6_time` varchar(255) DEFAULT NULL,
  `process7_time` varchar(255) DEFAULT NULL,
  `process8_time` varchar(255) DEFAULT NULL,
  `process9_time` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `average` varchar(255) DEFAULT NULL,
  `statuslists` varchar(255) DEFAULT NULL,
  `applieddate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vaccumetable`
--

INSERT INTO `vaccumetable` (`id`, `date`, `shift`, `machine_Sl_No`, `checked_by`, `pressure_guage_value`, `process1_result`, `process2_result`, `process3_result`, `process4_result`, `process5_result`, `process6_result`, `process7_result`, `process8_result`, `process9_result`, `process1_time`, `process2_time`, `process3_time`, `process4_time`, `process5_time`, `process6_time`, `process7_time`, `process8_time`, `process9_time`, `description`, `status`, `average`, `statuslists`, `applieddate`) VALUES
(1, '2021-09-13', 'Shift B', 'ggfd455454', 'kalidas', '150mm/Hg ', 'Yes', 'No', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', '2', '2', '2', '2', '2', '3', '0', '2', '9', 'Not Provided', 'In Complete', '6/9', 'step2,step4,step6', '2021-09-15 04:57:03'),
(2, '2021-09-15', 'Shift A', 'ggfd455454', 'kalidas 1', '150mm/Hg ', 'Yes', 'No', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', '2', '2', '2', '2', '2', '3', '0', '2', '9', 'Not Provided', 'Complete', '6/9', 'step2,step4,step6', '2021-09-15 05:05:35'),
(3, '2021-09-15', 'Shift B', 'ggfd455454', 'kalidas 2', '150mm/Hg ', 'Yes', 'No', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', '2', '2', '2', '2', '2', '3', '0', '2', '9', 'Not Provided', 'In Complete', '6/9', 'step2,step4,step6', '2021-09-15 04:51:16'),
(4, '2021-09-15', 'Shift B', 'ggfd455454', 'kalidas 3', '150mm/Hg ', 'Yes', 'No', 'Yes', 'No', 'Yes', 'No', 'Yes', 'Yes', 'Yes', '2', '2', '2', '2', '2', '3', '0', '2', '9', 'Not Provided', 'In Complete', '6/9', 'step2,step4,step6', '2021-09-15 04:51:19');

-- --------------------------------------------------------

--
-- Table structure for table `vaccume_heading`
--

CREATE TABLE `vaccume_heading` (
  `id` int(255) NOT NULL,
  `heading` varchar(256) NOT NULL,
  `sub_heading` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `machine_serial_no`
--
ALTER TABLE `machine_serial_no`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otatable`
--
ALTER TABLE `otatable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ota_heading`
--
ALTER TABLE `ota_heading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pvatable`
--
ALTER TABLE `pvatable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pva_heading`
--
ALTER TABLE `pva_heading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `solderingtable`
--
ALTER TABLE `solderingtable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thermaltable`
--
ALTER TABLE `thermaltable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uwatable`
--
ALTER TABLE `uwatable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uwa_heading`
--
ALTER TABLE `uwa_heading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vaccumetable`
--
ALTER TABLE `vaccumetable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vaccume_heading`
--
ALTER TABLE `vaccume_heading`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `machine_serial_no`
--
ALTER TABLE `machine_serial_no`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `otatable`
--
ALTER TABLE `otatable`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ota_heading`
--
ALTER TABLE `ota_heading`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pvatable`
--
ALTER TABLE `pvatable`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pva_heading`
--
ALTER TABLE `pva_heading`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `solderingtable`
--
ALTER TABLE `solderingtable`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `thermaltable`
--
ALTER TABLE `thermaltable`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `uwatable`
--
ALTER TABLE `uwatable`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vaccumetable`
--
ALTER TABLE `vaccumetable`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
